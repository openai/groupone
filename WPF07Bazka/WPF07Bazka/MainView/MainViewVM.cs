﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace WPF07Bazka.MainView
{
    public class MainViewVM : BindableBase
    {
        private PotatoesManager _manager;

        public MainViewVM()
        {
            _manager = new PotatoesManager();
                     
            GenerateCommand = new DelegateCommand(() =>
            {
                _manager.Income(PotatoesGenerator.Generate(30));
            });
            
            Cart = new ObservableCollection<PotatoViewModel>();
            ToKeepCart = new ObservableCollection<PotatoViewModel>();
            ToRecycleCart = new ObservableCollection<PotatoViewModel>();

            SyncCollections(_manager.Cart, Cart);
            SyncCollections(_manager.KeepCart, ToKeepCart);
            SyncCollections(_manager.RecycleCart, ToRecycleCart);
        }

        private void SyncCollections(ReadOnlyObservableCollection<Potato> toWatchCollection,
            ObservableCollection<PotatoViewModel> myCollection)
        {
            ((INotifyCollectionChanged)toWatchCollection).CollectionChanged +=
                (s, e) =>
                {
                    if (e.NewItems != null)
                        foreach (var p in e.NewItems)
                            myCollection.Add(new PotatoViewModel((Potato)p, this));

                    if (e.OldItems != null)
                        foreach (var p in e.OldItems)
                            myCollection.Remove(myCollection.First(mp => mp.MyPotato == p));
                };
        }

        public ObservableCollection<PotatoViewModel> Cart { get; set; }
        public ObservableCollection<PotatoViewModel> ToKeepCart { get; set; }
        public ObservableCollection<PotatoViewModel> ToRecycleCart { get; set; }
        public DelegateCommand GenerateCommand { get; set; }

        internal void KeepPotato(PotatoViewModel potatoViewModel)
        {
            _manager.KeepPotato(potatoViewModel.MyPotato);
        }

        internal void RecyclePotato(PotatoViewModel potatoViewModel)
        {
            _manager.DeletePotato(potatoViewModel.MyPotato);
        }
    }

    public class PotatoViewModel
    {
        public PotatoViewModel(Potato p, MainViewVM parent)
        {
            _potato = p;
            _parent = parent;
            KeepCommand = new DelegateCommand(() => {
                _parent.KeepPotato(this);
            });
            RecycleCommand = new DelegateCommand(() => {
                _parent.RecyclePotato(this);
            });
        }

        public Visibility ButtonsVisible { get; set; }

        public Size Size => _potato.Size;
        public int Rotenness => _potato.Rottenness;
        public string RotennessStr { get {
                return Rotenness.ToString() + "%";
            }
        }
        public BitmapImage MyImage {
            get
            {
                if (Size == Size.Кингсайз)
                    return new BitmapImage(new Uri(@"/Images/king of the potatoes.jpg", UriKind.Relative));
                if (Rotenness < 50)
                    return new BitmapImage(new Uri(@"/Images/bad potato.png", UriKind.Relative));
                return new BitmapImage(new Uri(@"/Images/common potatoe.png", UriKind.Relative));
            }
        }

        public DelegateCommand KeepCommand { get; set; }
        public DelegateCommand RecycleCommand { get; set; }    
        private readonly Potato _potato;
        private MainViewVM _parent;
        public Potato MyPotato => _potato;
    }

    public class Potato
    {
        public Size Size;
        public int Rottenness;        
    }

    public class PotatoesManager
    {
        public ReadOnlyObservableCollection<Potato> Cart;            
        ObservableCollection<Potato> _cart = new ObservableCollection<Potato>();

        public ReadOnlyObservableCollection<Potato> KeepCart;
        ObservableCollection<Potato> _keepCart = new ObservableCollection<Potato>();

        public ReadOnlyObservableCollection<Potato> RecycleCart;
        ObservableCollection<Potato> _recycleCart = new ObservableCollection<Potato>();

        public PotatoesManager()
        {
            Cart = new ReadOnlyObservableCollection<Potato>(_cart);
            KeepCart = new ReadOnlyObservableCollection<Potato>(_keepCart);
            RecycleCart = new ReadOnlyObservableCollection<Potato>(_recycleCart);
        }

        public void Income(IEnumerable<Potato> potatoes)
        {
            _cart.AddRange(potatoes);
        }

        public void KeepPotato(Potato potato)
        {
            _cart.Remove(potato);
            _keepCart.Add(potato);
        }

        public void DeletePotato(Potato potato)
        {
            _cart.Remove(potato);
            _recycleCart.Add(potato);
        }
    }

    public enum Size
    {
        Крошечная, Мелкая, Нормальная, Крупная, Гигантская, Кингсайз
    }

    public static class PotatoesGenerator
    {
        public static IEnumerable<Potato> Generate(int count)
        {
            Random r = new Random();
            return Enumerable.Range(0, count).Select(e => new Potato()
            {
                Size = (Size)r.Next(6),
                Rottenness = r.Next(101)
            });
        }
    }
}
