﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dzShapes
{
    class Program
    {
        static void Main()
        {
            var objs = new List<Shape>();
            for (int i = 0; i < 10; ++i)
                objs.Add(ShapeGenerator.GenerateRandomShape(10, 5));
            ShapeArranger.ToGrid(4, 3, objs);
            ShapeDrawer.DrawShapesAt(5, 5, objs);
            Console.ReadLine();
        }
    }

    internal static class ShapeDrawer
    {
        public static void DrawShapesAt(int left, int top, List<Shape> objs)
        {
            foreach (var shape in objs)
            {
                List<Point> points = shape.GetPoints();
                foreach (var point in points)
                    DrawPoint(new Point(point.X + left, point.Y + top));
            }
        }

        private static void DrawPoint(Point point)
        {
            if (point.X > 79 || point.X < 0) return;
            if (point.Y > 24 || point.Y < 0) return;
            Console.SetCursorPosition(point.X, point.Y);
            Console.Write('*');
        }
    }

    internal class Point
    {
        public int X;
        public int Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
    }

    internal static class ShapeArranger
    {
        public static void ToGrid(int cols, int rows, List<Shape> objs)
        {
            var maxWidth = objs.Max(o => o.Width);
            var maxHeght = objs.Max(o => o.Height);
            int curObj = 0;

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; ++c)
                {
                    if (curObj >= objs.Count) return;
                    objs[curObj].Left = c * (maxWidth + 3);
                    objs[curObj].Top = r * (maxHeght + 2);
                    ++curObj;
                }
            }
        }
    }

    internal static class ShapeGenerator
    {
        static Random rnd = new Random();
        public static Shape GenerateRandomShape(int width, int height)
        {
            var r = rnd.Next(3);
            switch (r)
            {
                case 0:
                    return new Rectangle(width, height);
                case 1:
                    return new Shveller(width, height);
                case 2:
                    return new Tavr(width, height);
            }
             throw new NotImplementedException();
        }
    }

    internal abstract class Shape
    {
        public int Width { get; }
        public int Height { get; }
        public int Top { get; set; }
        public int Left { get; set; }

        public Shape(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public abstract List<Point> GetPoints();
    }

    class Rectangle : Shape
    {
        public Rectangle(int width, int height) : base(width, height)
        {
        }

        public override List<Point> GetPoints()
        {
            List<Point> points = new List<Point>();
            for (int x = 0; x < Width; x++)
            {
                points.Add(new Point(Left + x, Top));
                points.Add(new Point(Left + x, Top + Height));
            }
            for (int y = 0; y < Height; y++)
            {
                points.Add(new Point(Left, Top + y));
                points.Add(new Point(Left + Width, Top + y));
            }
            return points;
        }
    }

    class Shveller : Shape
    {
        public Shveller(int width, int height) : base(width, height)
        {
        }

        public override List<Point> GetPoints()
        {
            List<Point> points = new List<Point>();
            for (int x = 0; x < Width; x++)
            {
                points.Add(new Point(Left + x, Top + Height));
            }
            for (int y = 0; y < Height; y++)
            {
                points.Add(new Point(Left, Top + y));
                points.Add(new Point(Left + Width, Top + y));
            }
            return points;
        }
    }

    class Tavr : Shape
    {
        public Tavr(int width, int height) : base(width, height)
        {
        }

        public override List<Point> GetPoints()
        {
            List<Point> points = new List<Point>();
            for (int x = 0; x < Width; x++)
            {
                points.Add(new Point(Left + x, Top + Height));
            }
            for (int y = 0; y < Height; y++)
            {
                points.Add(new Point(Left + Width / 2, Top + y));
            }
            return points;
        }
    }
}
