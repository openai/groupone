﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dev.Domain;

namespace Dev.Domain
{
    public class Developer
    {
        public int DeveloperId { get; set; }
        public string DevName { get; set; }
        public string DevLastName { get; set; }
    }
}
