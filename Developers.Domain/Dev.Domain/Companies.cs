﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dev.Domain;

namespace Dev.Domain
{
   public class Company
    {
        public int CompanyId { get; set; }
        public string ComName { get; set; }
    }
}
