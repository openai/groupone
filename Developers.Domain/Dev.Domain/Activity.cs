﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dev.Domain
{
    public class Activity
    {
        public int ActivityId { get; set; }
        public string Caption { get; set; }
    }
}