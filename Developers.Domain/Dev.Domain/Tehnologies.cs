﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dev.Domain;

namespace Dev.Domain
{
    public class Tehnology
    {
        public int TehnologyId { get; set; }
        public string TehName { get; set; }
        public override string ToString()
        {
            return TehName;
        }
    }
}
