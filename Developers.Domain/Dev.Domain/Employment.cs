﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dev.Domain
{
   public class Employment
    {
        public int EmploymentId { get; set; }
        public Developer DeveloperId { get; set; }
        public Company CompanyId { get; set; }
        public DateTime DateAddmission { get; set; }
        public DateTime DateDismissal { get; set; }
        public Tehnology TehnologyId { get; set; }
    }
}
