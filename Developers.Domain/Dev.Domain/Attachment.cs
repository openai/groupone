﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dev.Domain;

namespace Dev.Domain
{
    public class Attachment
    {
        public int ActivityId { get; set; }
        public int CompanyId { get; set; }
        public Activity Activity { get; set; }
        public Company Company { get; set; }
    }
}
