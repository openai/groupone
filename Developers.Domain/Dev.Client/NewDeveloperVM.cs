﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Dev.Domain;
using Prism.Commands;

namespace Dev.Client
{
    public class NewDeveloperVM
    {
        public Developer Developer { get; set; }
        public NewDeveloperVM()
        {
            Developer = new Developer();
            AddCommand = new DelegateCommand<NewDeveloper>(wnd =>
            {
                wnd.Developer = Developer;
                wnd.DialogResult = true;
                wnd.Close();
            });
        }

        public DelegateCommand<NewDeveloper> AddCommand { get; set; }
    }
}
