﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Dev.Data;
using Dev.Domain;
using System.Data.Entity.Core.Objects;

namespace Dev.Client
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DeveloperContext context;
        public MainWindow()
        {
            InitializeComponent();
            context = new DeveloperContext();
        }

        private void GenerateDevelopers(object sender, RoutedEventArgs e)
        {
            context.Database.ExecuteSqlCommand($"DELETE FROM [{nameof(context.Employments)}]");
            context.Database.ExecuteSqlCommand($"DELETE FROM [{nameof(context.Companies)}]");
            context.Database.ExecuteSqlCommand($"DELETE FROM [{nameof(context.Attachments)}]");
            context.Database.ExecuteSqlCommand($"DELETE FROM [{nameof(context.Activities)}]");
            context.Database.ExecuteSqlCommand($"DELETE FROM [{nameof(context.Developers)}]");
            context.Database.ExecuteSqlCommand($"DELETE FROM [{nameof(context.Tehnologies)}]");

            var model = new Model();
            model.Generate();
            model.GenerateDevelopers();
            model.GenerateEmployments();

            context.Activities.Add(model.It);
            context.Activities.Add(model.Auto);
            context.Activities.Add(model.AutoService);
            context.Activities.Add(model.Retail);
            context.Activities.Add(model.Smi);
            context.SaveChanges();

            context.Companies.AddRange(model.Companies);
            context.SaveChanges();
            context.Tehnologies.AddRange(model.Tehnologies);
            context.SaveChanges();
            context.Developers.AddRange(model.Developers);
            context.SaveChanges();
            context.Attachments.AddRange(model.Atachments);
            context.SaveChanges();
            context.Employments.AddRange(model.Employments);
            context.SaveChanges();

        }
        
        public class Model
        {
            public Activity It = new Activity() {Caption = "IT-tehnology"};
            public Activity Auto = new Activity() {Caption = "Автомобили"};
            public Activity AutoService = new Activity() {Caption = "Автосервис"};
            public Activity Retail = new Activity() {Caption = "Торговля"};
            public Activity Smi = new Activity() {Caption = "СМИ"};

            public List<Company> Companies = new List<Company>()
            {
                new Company() {ComName = "ООО Рога и Копыта"},
                new Company() {ComName = "ПАО Сбербанк"},
                new Company() {ComName = "Acronis inc"},
                new Company() {ComName = "ИП Мамедов"},
                new Company() {ComName = "ЗАО Аэрофлот"}
            };

            public List<Tehnology> Tehnologies = new List<Tehnology>()
            {
                new Tehnology() {TehName = "WEB"},
                new Tehnology() {TehName = "WPF"},
                new Tehnology() {TehName = "SQL"},
                new Tehnology() {TehName = "HTML"},
                new Tehnology() {TehName = "JAVA"}
            };

            public List<Developer> Developers = new List<Developer>();
            public List<Attachment> Atachments = new List<Attachment>();
            public List<Employment> Employments = new List<Employment>();

            public void Generate()
            {
                Atachments.Add(new Attachment() {Company = Companies[0], Activity = It});
                Atachments.Add(new Attachment() {Company = Companies[0], Activity = Smi});
                Atachments.Add(new Attachment() {Company = Companies[1], Activity = Auto});
                Atachments.Add(new Attachment() {Company = Companies[1], Activity = AutoService});
                Atachments.Add(new Attachment() {Company = Companies[2], Activity = Retail});
                Atachments.Add(new Attachment() {Company = Companies[2], Activity = It});
                Atachments.Add(new Attachment() {Company = Companies[3], Activity = AutoService});
                Atachments.Add(new Attachment() {Company = Companies[3], Activity = Smi});
                Atachments.Add(new Attachment() {Company = Companies[4], Activity = Retail});
                Atachments.Add(new Attachment() {Company = Companies[4], Activity = Auto});
            }

            public void GenerateEmployments()
            {
                Random r = new Random();
                foreach(var dev in Developers)
                {
                    GenerateEmploymentsForDeveloper(dev, r);
                }
            }
            public void GenerateEmploymentsForDeveloper(Developer dev, Random r)
            {
                DateTime lastDate = Convert.ToDateTime("01/01/2000");
                List<DateTime> dates = new List<DateTime>();
                while ((lastDate = lastDate.AddDays(r.Next(1730) + 100)) < DateTime.Now)
                {
                    dates.Add(lastDate);
                }
                for (int i = 0; i != dates.Count - 1; i++)
                {
                    Employments.Add(new Employment
                    {
                        DateAddmission = dates[i].AddDays(r.Next(10)),
                        DateDismissal = dates[i + 1],
                        CompanyId = Companies[r.Next(Companies.Count)],
                        TehnologyId = Tehnologies[r.Next(Tehnologies.Count)],
                        DeveloperId = dev
                    });
                }
            }
            public void GenerateDevelopers()
            {
                Random r = new Random();
                for(int i = 0; i != 10; ++i)
                {                    
                    Developers.Add(new Developer()
                    {
                        DevName = GenerateName(r),
                        DevLastName = GenerateName(r)
                    });
                }
            }
            public static string GenerateName(Random r)
            {
                int len = r.Next(5)+4;
                string[] consonants = { "б", "в", "г", "д", "ж", "з", "к", "л", "м", "н", "п", "р", "с", "т", "ф", "ц", "ч"};
                string[] vowels = { "о", "е", "и", "а", "у" };
                string Name = "";
                Name += consonants[r.Next(consonants.Length)].ToUpper();
                Name += vowels[r.Next(vowels.Length)];
                int b = 2; //b tells how many times a new letter has been added. It's 2 right now because the first two letters are already in the name.
                while (b < len)
                {
                    Name += consonants[r.Next(consonants.Length)];
                    b++;
                    Name += vowels[r.Next(vowels.Length)];
                    b++;
                }

                return Name;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs args)
        {
            var res = context.Employments
                .GroupBy(e => new { dev = e.DeveloperId, tech = e.TehnologyId.TehName })
                .Select(e => new
                {
                    e.Key.dev.DevName,
                    e.Key.dev.DevLastName,
                    e.Key.tech,
                    time = e.Where(r => r.DeveloperId == e.Key.dev && r.TehnologyId.TehName == e.Key.tech)  //EntityFunctions.DiffDays(e.DateAddmission, e.DateDismissal) })
                .Select(dd => EntityFunctions.DiffDays(dd.DateAddmission, dd.DateDismissal))
                .Sum()
                })
                .OrderBy(o => o.DevLastName)
                //.GroupBy(e => new { Name = e.Key.dev.DevName, Surname = e.Key.dev.DevLastName, Tech = e.Key.tech, Time = e.Sum(e.Key.time ?? default(int))})
                .ToList();
            TheGrid.ItemsSource = res;
        }

        private void Button_Click_1(object sender, RoutedEventArgs args)
        {
            var res = context.Employments
                .Select(e => new
                {
                    dev = e,
                    others = context.Employments
                        .Where(r => (r.DateAddmission >= e.DateAddmission && r.DateAddmission <= e.DateDismissal) && r.CompanyId == e.CompanyId && r.TehnologyId == e.TehnologyId && r.DeveloperId != e.DeveloperId)
                })
                .Where(e => e.others.Count() > 0)
                .SelectMany(e => e.others
                    .Select(r => new
                    {
                        dev1 = e.dev.DeveloperId.DevLastName,
                        dev2 = r.DeveloperId.DevLastName,
                        Company = e.dev.CompanyId.ComName,
                        Tech = e.dev.TehnologyId.TehName,
                        StartDate = e.dev.DateAddmission > r.DateAddmission ? e.dev.DateAddmission : r.DateAddmission,
                        EndDate = e.dev.DateDismissal < r.DateDismissal ? e.dev.DateDismissal : r.DateDismissal
                    }
                    ))
                .ToList();
            TheGrid.ItemsSource = res;

        }
    }
}
