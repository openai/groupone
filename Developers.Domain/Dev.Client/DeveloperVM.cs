﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dev.Data;
using Dev.Domain;
using Prism.Commands;
using Prism.Mvvm;

namespace Dev.Client
{
    public class DeveloperVM : BindableBase
    {
        private DeveloperContext _context;
        private string Log = String.Empty;
        

        public DeveloperVM()
        {
            _context = new DeveloperContext();
            _context.Database.Log = s => { Log += s; };
            UpdateDevelopers();
            DeveloperBestIn = new List<Tehnology>();
            SaveCommand = new DelegateCommand(() =>
            {
                _context.SaveChanges();
            });
            AddCommand = new DelegateCommand(() =>
            {
                var dlg = new NewDeveloper();
                if (dlg.ShowDialog() == true)
                {
                    _context.Developers.Add(dlg.Developer);
                    _context.SaveChanges();
                }
            });
        }

        public IEnumerable<Developer> DevelopersList { get; set; }
        public List<Tehnology> DeveloperBestIn { get; set; }

        private bool _topOnly;
        public bool TopOnly
        {
            get { return _topOnly; }
            set
            {
                _topOnly = value;
                UpdateDevelopers();
            }
        }

        public DelegateCommand SaveCommand { get; set; }
        public DelegateCommand AddCommand { get; set; }

        void UpdateDevelopers()
        {
            DevelopersList = _topOnly
                ? _context.GetTopDevelopers()
                : _context.GetDevelopers();
            RaisePropertyChanged(nameof(DevelopersList));
        }

        private Developer _currentDeveloper;
        

        public Developer CurrentDeveloper
        {
            get { return _currentDeveloper; }
            set
            {
                _currentDeveloper = value;
                if (value == null) return;
                DeveloperBestIn = _context
                    .GetTechDeveloperBestIn(value.DeveloperId).ToList();
                RaisePropertyChanged(nameof(DeveloperBestIn));
            }
        }
    }
}
