﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Dev.Domain;

namespace Dev.Client
{
    /// <summary>
    /// Interaction logic for NewDeveloper.xaml
    /// </summary>
    public partial class NewDeveloper : Window
    {
        public NewDeveloper()
        {
            InitializeComponent();
            DataContext = new NewDeveloperVM();
        }

        public Developer Developer { get; set; }

    }
}
