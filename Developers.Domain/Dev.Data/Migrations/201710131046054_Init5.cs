namespace Dev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Employments",
                c => new
                    {
                        EmploymentId = c.Int(nullable: false, identity: true),
                        DateAddmission = c.DateTime(nullable: false),
                        DateDismissal = c.DateTime(nullable: false),
                        CompanyId_CompanyId = c.Int(),
                        DeveloperId_DeveloperId = c.Int(),
                        TehnologyId_TehnologyId = c.Int(),
                    })
                .PrimaryKey(t => t.EmploymentId)
                .ForeignKey("dbo.Companies", t => t.CompanyId_CompanyId)
                .ForeignKey("dbo.Developers", t => t.DeveloperId_DeveloperId)
                .ForeignKey("dbo.Tehnologies", t => t.TehnologyId_TehnologyId)
                .Index(t => t.CompanyId_CompanyId)
                .Index(t => t.DeveloperId_DeveloperId)
                .Index(t => t.TehnologyId_TehnologyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employments", "TehnologyId_TehnologyId", "dbo.Tehnologies");
            DropForeignKey("dbo.Employments", "DeveloperId_DeveloperId", "dbo.Developers");
            DropForeignKey("dbo.Employments", "CompanyId_CompanyId", "dbo.Companies");
            DropIndex("dbo.Employments", new[] { "TehnologyId_TehnologyId" });
            DropIndex("dbo.Employments", new[] { "DeveloperId_DeveloperId" });
            DropIndex("dbo.Employments", new[] { "CompanyId_CompanyId" });
            DropTable("dbo.Employments");
        }
    }
}
