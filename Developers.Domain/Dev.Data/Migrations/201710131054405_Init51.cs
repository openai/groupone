namespace Dev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init51 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Companies", "ActivityId_ActivityId", "dbo.Activities");
            DropForeignKey("dbo.Companies", "ehId_TehnologyId", "dbo.Tehnologies");
            DropForeignKey("dbo.Developers", "ComId_CompanyId", "dbo.Companies");
            DropIndex("dbo.Companies", new[] { "ActivityId_ActivityId" });
            DropIndex("dbo.Companies", new[] { "ehId_TehnologyId" });
            DropIndex("dbo.Developers", new[] { "ComId_CompanyId" });
            DropColumn("dbo.Companies", "ActivityId_ActivityId");
            DropColumn("dbo.Companies", "ehId_TehnologyId");
            DropColumn("dbo.Developers", "ComId_CompanyId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Developers", "ComId_CompanyId", c => c.Int());
            AddColumn("dbo.Companies", "ehId_TehnologyId", c => c.Int());
            AddColumn("dbo.Companies", "ActivityId_ActivityId", c => c.Int());
            CreateIndex("dbo.Developers", "ComId_CompanyId");
            CreateIndex("dbo.Companies", "ehId_TehnologyId");
            CreateIndex("dbo.Companies", "ActivityId_ActivityId");
            AddForeignKey("dbo.Developers", "ComId_CompanyId", "dbo.Companies", "CompanyId");
            AddForeignKey("dbo.Companies", "ehId_TehnologyId", "dbo.Tehnologies", "TehnologyId");
            AddForeignKey("dbo.Companies", "ActivityId_ActivityId", "dbo.Activities", "ActivityId");
        }
    }
}
