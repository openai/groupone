namespace Dev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Attachments", "TehnologyId", "dbo.Tehnologies");
            DropIndex("dbo.Attachments", new[] { "TehnologyId" });
            DropPrimaryKey("dbo.Attachments");
            AddColumn("dbo.Attachments", "CompanyId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Attachments", new[] { "CompanyId", "ActivityId" });
            CreateIndex("dbo.Attachments", "CompanyId");
            AddForeignKey("dbo.Attachments", "CompanyId", "dbo.Companies", "CompanyId", cascadeDelete: true);
            DropColumn("dbo.Attachments", "TehnologyId");
            DropColumn("dbo.Companies", "DateAddmission");
            DropColumn("dbo.Companies", "DateDismissal");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Companies", "DateDismissal", c => c.DateTime(nullable: false));
            AddColumn("dbo.Companies", "DateAddmission", c => c.DateTime(nullable: false));
            AddColumn("dbo.Attachments", "TehnologyId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Attachments", "CompanyId", "dbo.Companies");
            DropIndex("dbo.Attachments", new[] { "CompanyId" });
            DropPrimaryKey("dbo.Attachments");
            DropColumn("dbo.Attachments", "CompanyId");
            AddPrimaryKey("dbo.Attachments", new[] { "TehnologyId", "ActivityId" });
            CreateIndex("dbo.Attachments", "TehnologyId");
            AddForeignKey("dbo.Attachments", "TehnologyId", "dbo.Tehnologies", "TehnologyId", cascadeDelete: true);
        }
    }
}
