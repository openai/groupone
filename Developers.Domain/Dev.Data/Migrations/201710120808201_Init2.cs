namespace Dev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attachments",
                c => new
                    {
                        TehnologyId = c.Int(nullable: false),
                        ActivityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TehnologyId, t.ActivityId })
                .ForeignKey("dbo.Activities", t => t.ActivityId, cascadeDelete: true)
                .ForeignKey("dbo.Tehnologies", t => t.TehnologyId, cascadeDelete: true)
                .Index(t => t.TehnologyId)
                .Index(t => t.ActivityId);
            
            CreateTable(
                "dbo.Tehnologies",
                c => new
                    {
                        TehnologyId = c.Int(nullable: false, identity: true),
                        TehName = c.String(),
                    })
                .PrimaryKey(t => t.TehnologyId);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        CompanyId = c.Int(nullable: false, identity: true),
                        ComName = c.String(),
                        DateAddmission = c.DateTime(nullable: false),
                        DateDismissal = c.DateTime(nullable: false),
                        ActivityId_ActivityId = c.Int(),
                        ehId_TehnologyId = c.Int(),
                    })
                .PrimaryKey(t => t.CompanyId)
                .ForeignKey("dbo.Activities", t => t.ActivityId_ActivityId)
                .ForeignKey("dbo.Tehnologies", t => t.ehId_TehnologyId)
                .Index(t => t.ActivityId_ActivityId)
                .Index(t => t.ehId_TehnologyId);
            
            CreateTable(
                "dbo.Developers",
                c => new
                    {
                        DeveloperId = c.Int(nullable: false, identity: true),
                        DevName = c.String(),
                        DevLastName = c.String(),
                        ComId_CompanyId = c.Int(),
                    })
                .PrimaryKey(t => t.DeveloperId)
                .ForeignKey("dbo.Companies", t => t.ComId_CompanyId)
                .Index(t => t.ComId_CompanyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Developers", "ComId_CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Companies", "ehId_TehnologyId", "dbo.Tehnologies");
            DropForeignKey("dbo.Companies", "ActivityId_ActivityId", "dbo.Activities");
            DropForeignKey("dbo.Attachments", "TehnologyId", "dbo.Tehnologies");
            DropForeignKey("dbo.Attachments", "ActivityId", "dbo.Activities");
            DropIndex("dbo.Developers", new[] { "ComId_CompanyId" });
            DropIndex("dbo.Companies", new[] { "ehId_TehnologyId" });
            DropIndex("dbo.Companies", new[] { "ActivityId_ActivityId" });
            DropIndex("dbo.Attachments", new[] { "ActivityId" });
            DropIndex("dbo.Attachments", new[] { "TehnologyId" });
            DropTable("dbo.Developers");
            DropTable("dbo.Companies");
            DropTable("dbo.Tehnologies");
            DropTable("dbo.Attachments");
        }
    }
}
