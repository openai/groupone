﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Dev.Domain;

namespace Dev.Data
{
    public class DeveloperContext : DbContext
    {
        public DbSet<Activity> Activities { get; set; }
        public DbSet<Tehnology> Tehnologies { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<Developer> Developers { get; set; }
        public DbSet<Employment> Employments { get; set; }

        public DeveloperContext() : base("Data Source=.;Initial Catalog=DeveloperContext;Integrated Security=True")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Attachment>()
                .HasKey(attachment => new { attachment.CompanyId, attachment.ActivityId });
            base.OnModelCreating(modelBuilder);
        }


        public class Experience
        {
            public Developer Developer { get; set; }
            public Tehnology Technology { get; set; }
            public int? Days { get; set; }
        }
        public IQueryable<Experience> GetAllExperience()
        {
            var res = Employments
                .GroupBy(e => new {dev = e.DeveloperId, tech = e.TehnologyId})
                .Select(e => new Experience()
                {
                    Developer = e.Key.dev,
                    Technology = e.Key.tech,
                    Days = e
                        .Where(r => r.DeveloperId == e.Key.dev &&
                                    r.TehnologyId ==
                                    e.Key.tech) 
                        .Select(dd => EntityFunctions.DiffDays(dd.DateAddmission, dd.DateDismissal))
                        .Sum()
                })
                .OrderBy(o => o.Developer.DevLastName);

            return res;
        }

        public IEnumerable<Developer> GetTopDevelopers()
        {
            var result = new List<Developer>();

            var allExperience = GetAllExperience();

            var groups = allExperience.GroupBy(g => g.Technology);
            foreach (var gr in groups)
            {
                var row = gr.OrderByDescending(o => o.Days).FirstOrDefault();
                if(row != null)
                    result.Add(row.Developer);
            }

            return result;
        }

        public IEnumerable<Developer> GetDevelopers()
        {
            if (Developers.Local.Count == 0)
            {
                var tmp = Developers.ToList();
            }
            return Developers.Local;
        }


        public IEnumerable<Tehnology> GetTechDeveloperBestIn(int developerId)
        {
            var allExperience = GetAllExperience().ToList();
            var devExperience = allExperience.Where(d => d.Developer.DeveloperId == developerId);

            List<Tehnology> result = new List<Tehnology>();
            foreach (var exp in devExperience)
            {
                if (exp.Days == allExperience
                    .Where(e => e.Technology == exp.Technology).Max(r => r.Days))
                    result.Add(exp.Technology);
            }

            return result;
        }
    }

}
