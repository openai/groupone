﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecursiveTree
{
    public class Node
    {
        public List<Node> Children;
        public static int NodeCount;
        public const int NodeMax = 30;
        private Node()
        {
            Children = new List<Node>();
            ++NodeCount;
        }

        public static Node CreateNode(string name)
        {
            if (NodeCount >= NodeMax) return null;
            Node node = new Node {Name = name};
            return node;
        }

        public string Name;

        public void DrawNode()
        {
            Console.WriteLine(Name);
            foreach (var child in Children)
                child.DrawNode();
        }

        public int Count()
        {
            int count = 1;
            foreach (var child in Children)
                count += child.Count();
            return count;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            Node root = Node.CreateNode("root"); 
            if (root == null) return;

            int childCount = r.Next(5);
            for (int i = 0; i < childCount; ++i)
            {
                var node = Node.CreateNode((i+1).ToString());
                if (node == null) break;
                FillNode(node, r, 1);
                root.Children.Add(node);
            }

            root.DrawNode();
            Console.WriteLine("Node count = {0}", root.Count());
            Console.ReadKey();
        }

        private static void FillNode(Node node, Random rnd, int generation)
        {
            if (generation == 5) return;
            int childCount = rnd.Next(5);
            for (int i = 0; i < childCount; ++i)
            {
                var child = Node.CreateNode(node.Name + "." + (i + 1));
                if (child == null) return;
                node.Children.Add(child);
                FillNode(child, rnd, generation + 1);
            }
        }
    }
}
