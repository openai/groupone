﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfVisualization
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, ICanvas
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainVM(this);
        }

        public void DrawCircle(double x, double y, double radius)
        {
            var circle = new Ellipse()
            {
                Stroke = Brushes.Black,
                StrokeThickness = 3.0,
                Fill = Brushes.Cyan,
                Width = radius * 2,
                Height = radius * 2
            };
            circle.SetValue(Canvas.LeftProperty, x - radius);
            circle.SetValue(Canvas.TopProperty, y - radius);
            TheCanvas.Children.Add(circle);
        }

        public void DrawLine(double x1, double y1, double x2, double y2)
        {
            var line = new Line()
            {
                Stroke = Brushes.Blue,
                StrokeThickness = 3,
                X1 = x1,
                Y1 = y1,
                X2 = x2,
                Y2 = y2
            };
            TheCanvas.Children.Add(line);
        }
    }
}
