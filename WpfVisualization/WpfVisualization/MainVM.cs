﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfVisualization
{
    public class MainVM
    {
        private readonly ICanvas _myCanvas;

        public MainVM(ICanvas myCanvas)
        {
            _myCanvas = myCanvas;
            //for(int i = 0; i < 10; ++i)
            //    _myCanvas.DrawCircle(50 + 50*i, 50, 24);

            //for (int i = 0; i <= 10; ++i)
            //{
            //    _myCanvas.DrawLine(10 + 10 * i, 100, 10 + 10 * i, 200);
            //    _myCanvas.DrawLine(10, 100 + 10 * i, 110, 100 + 10 * i);
            //}
            var gen = new TreeGen();
            var root = gen.GenerateTree(0);

        }
    }

    public interface ICanvas
    {
        void DrawCircle(double x, double y, double radius);
        void DrawLine(double x1, double y1, double x2, double y2);
    }

    public class TreeGen
    {
        private int _totalChildrenCount = 0;
        public const int MaxDirectChildren = 4;
        public const int MaxLevel = 5;
        public const int MaxChildren = 100;

        static Random r = new Random();
        public Node GenerateTree(int level)
        {
            var root = new Node();
            if (level >= MaxLevel) return root;
            var childrenCount = r.Next(MaxDirectChildren + 1);
            for (int i = 0; i < childrenCount; ++i)
            {
                if (_totalChildrenCount < MaxChildren)
                {
                    root.children.Add(GenerateTree(level+1));
                    ++_totalChildrenCount;
                }
            }
            return root;
        }
    }

    public class Node
    {
        public List<Node> children = new List<Node>();
    }
}
