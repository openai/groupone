﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using NotifyPrj.Annotations;

namespace NotifyPrj
{
    public class DatasModel : INotifyPropertyChanged
    {
        private string _name;

        public string Name { get { return _name; } }
        private List<double> _datas = new List<double>();
        
        public DatasModel(string name)
        {
            _name = name;
            Random r = new Random();
            _datas.AddRange(Enumerable.Range(0, 5000000).Select(d => r.NextDouble()));
        }

        public List<double> GetDatas()
        {
            return _datas;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void ShuffleData()
        {
            Random r = new Random();
            if (r.Next(5) != 4)
            {
                _name = _name + "1";
                OnPropertyChanged("Name");
            }
            else
            {
                _datas.Clear();
                _datas.AddRange(Enumerable.Range(0, 5000000).Select(d => r.NextDouble()));
                OnPropertyChanged("Datas");
            }
        }
    }

    public class DatasViewModel 
    {
        private readonly DatasModel _model;

        public DatasViewModel(DatasModel model)
        {
            _model = model;
            model.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName == "Datas")
                {
                    Average = model.GetDatas().Average();
                    Max = model.GetDatas().Max();
                }
            };
        }

        public string Name => _model.Name;

        public double Average { get; set; }
        public double Max { get; set; }
    }

    public abstract class ViewBase
    {
        public abstract void Render();
    }

    public class ViewAverage : ViewBase
    {
        public DatasViewModel DataContext;
        public override void Render()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"{DataContext.Name} {DataContext.Average}");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }

    public class ViewMax : ViewBase
    {
        public DatasViewModel DataContext;
        public override void Render()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"{DataContext.Name} {DataContext.Max}");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<DatasModel> models = Enumerable.Range(0, 5)
                .Select(e => new DatasModel("Model " + e.ToString()))
                .ToList();
            List<DatasViewModel> viewModels = Enumerable.Range(0, 5)
                .Select(e => new DatasViewModel(models[e]))
                .ToList();
            List<ViewBase> views = new List<ViewBase>();
            views.AddRange(Enumerable.Range(0, 5)
                .Select(e => new ViewAverage()
                {
                    DataContext = viewModels[e]
                }));
            views.AddRange(Enumerable.Range(0, 5)
                .Select(e => new ViewMax()
                {
                    DataContext = viewModels[e]
                }));
            char ch;


            while ((ch = Console.ReadKey().KeyChar) != '0')
            {
                if (!"012345".Contains(ch)) continue;
                int el = int.Parse(ch.ToString());
                if (el == 0) break;
                models[el - 1].ShuffleData();
                Console.Clear();
                foreach (var view in views)
                  view.Render();
            }
        }
    }
}
