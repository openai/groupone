﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enumer
{
    class WeekDays : IEnumerable<string>
    {
        protected List<string> days = new List<string>
        {
            "ПН","ВТ","СР","ЧТ","ПТ","СБ","ВС",
        };
            
        public IEnumerator<string> GetEnumerator()
        {
            var en = new WeekDaysEnumerator(days);
            return en;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    class WeekDaysEnumerator : IEnumerator<string>
    {
        private readonly List<string> _weekDays;
        private int _cursor = -1;
        
        public WeekDaysEnumerator(List<string> weekDays)
        {
            _weekDays = weekDays;
        }

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            if (_cursor >= _weekDays.Count-1) return false;
            ++_cursor;
            return true;
        }

        public void Reset()
        {
            _cursor = -1;
        }

        public string Current => _weekDays[_cursor];

        object IEnumerator.Current => Current;
    }
    class Program
    {
        static void Main(string[] args)
        {
            var wd = new WeekDays();
            foreach (var weekDay in wd)
                Console.WriteLine(weekDay);
            foreach (var weekDay in wd.Reverse())
                Console.WriteLine(weekDay);
            Console.ReadKey();
        }
    }
}
