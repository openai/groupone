﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Prism.Commands;
using Prism.Mvvm;

namespace SchoolAdo.Main
{
    public class MainWindowVM : BindableBase
    {
        SchoolModel _model = new SchoolModel();
        private SchoolClass _currentClass;
        private List<Student> _students;
        private Student _currentStudent;

        public MainWindowVM()
        {
            Classes = _model.GetClasses();
            CurrentDay = new DateTime(2005, 4, 22);
        }
        public DateTime CurrentDay { get; set; }

        public DelegateCommand DayBackCommand { get; set; }
        
        public DelegateCommand DayForwadCommand { get; set; }

        public List<JournalLog> SubjectsOfTheDay
        {
            get
            {
                if(CurrentStudent != null)
                return _model.GetJournalLogs(CurrentDay, CurrentStudent.Id, CurrentClass.Id);
                return new List<JournalLog>();
            }
        }

        public List<SchoolClass> Classes { get; set; }

        public SchoolClass CurrentClass
        {
            get { return _currentClass; }
            set { _currentClass = value;
                RaisePropertyChanged(nameof(Students)); }
        }

        public List<Student> Students
        {
            get
            {
                if(CurrentClass == null)
                    return new List<Student>();
                return _model.GetStudentsByClass(CurrentClass.Id); 
            }
            set { _students = value; }
        }

        public Student CurrentStudent
        {
            get { return _currentStudent; }
            set
            {
                _currentStudent = value;
                RaisePropertyChanged(nameof(SubjectsOfTheDay));
            }
        }
    }

    public class Student
    {
        public string FullName { get; set; }
        public int Id { get; set; }
        public override string ToString()
        {
            return FullName;
        }
    }

    public class SchoolClass
    {
        public int Id { get; set; }
        public int Num { get; set; }
        public string Letter { get; set; }
        public override string ToString()
        {
            return Num.ToString() + " " + Letter;
        }
    }

    public class JournalLog
    {
        public string Subject { get; set; }
        public int? Grade { get; set; }
        public override string ToString()
        {
            return Subject + " " + (Grade.HasValue ? Grade.ToString() : "");
        }
    }
}
