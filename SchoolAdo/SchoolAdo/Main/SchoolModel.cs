﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SchoolAdo.Main
{
    internal class SchoolModel
    {
        public List<SchoolClass> GetClasses()
        {
            var result = new List<SchoolClass>();
            using (var con = GetSql())
            {
                SqlCommand getClasses = new SqlCommand(
                    @"SELECT [ClassId],[CreationDate],[Letter],[Number]
                FROM[School].[dbo].[Classes] where YEAR(CreationDate)=2004", con);
                using (SqlDataReader reader 
                    = getClasses.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var cl = new SchoolClass()
                        {
                            Id = (int)reader["ClassId"],
                             Letter = (string)reader["Letter"],
                             Num = (int)reader["Number"]
                        };
                        result.Add(cl);
                    }
                }
                return result;
            }
        }

        public List<Student> GetStudentsByClass(int classId)
        {
            var result = new List<Student>();
            using (var con = GetSql())
            {
                SqlCommand getClasses = new SqlCommand(
                    @"SELECT s.[StudentId]  as stid     
      ,[FirstName]      
      ,[LastName]
      ,[Patronymic]
  FROM [School].[dbo].[Students] s join StudentAttachments sa on sa.StudentId = s.StudentId
  where sa.ClassId = " + classId, con);
                using (SqlDataReader reader
                    = getClasses.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var cl = new Student()
                        {
                            FullName = $"{(string)reader["LastName"]}, {(string)reader["FirstName"]} {(string)reader["Patronymic"]}",
                            Id = (int)reader["stid"]
                        };
                        result.Add(cl);
                    }
                }
                return result;
            }
        }

        SqlConnection GetSql()
        {
            var result = 
                new SqlConnection("Server=.;Database=School;Trusted_Connection=True");
            result.Open();
            return result;
        }

        public List<JournalLog> GetJournalLogs(DateTime currentDay, int currentStudentId, int classId)
        {
            var result = new List<JournalLog>();
            using (var con = GetSql())
            {
                SqlCommand getClasses = new SqlCommand(
                    $@"SELECT sb.Name as subname, GradeValue  FROM  

Schedules sch left join [School].[dbo].[Grades] gr  
    on gr.ScheduleId = sch.ScheduleId and gr.StudentId = {currentStudentId}
	 join Subjects sb on sb.SubjectId = sch.SubjectId 

where sch.Date = '{currentDay.ToString("yyyy-M-d")}' and sch.ClassId = {classId}", con);

                using (SqlDataReader reader
                    = getClasses.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var cl = new JournalLog()
                        {
                            Subject = (string) reader["subname"],
                            Grade = reader["GradeValue"] == DBNull.Value
                                ? (int?) null
                                : (int) reader["GradeValue"]
                        };
                        result.Add(cl);
                    }
                }
                return result;
            }

            /*SELECT sb.Name, GradeValue  FROM  

Schedules sch left join [School].[dbo].[Grades] gr  
    on gr.ScheduleId = sch.ScheduleId and gr.StudentId = 22
	 join Subjects sb on sb.SubjectId = sch.SubjectId 

where sch.Date = '2005-4-22' and sch.ClassId = 1 */
        }
    }
}