﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTableSimple
{
    class HashiTable
    {
        private const int Cap = 10;
        private List<List<Person>> _array;

        public HashiTable()
        {
            _array = new List<List<Person>>();
            _array.AddRange(Enumerable.Range(0, Cap).Select(e => new List<Person>()));
        }

        public void Add(Person per)
        {
            int hash = per.GetHashCode() % Cap;
            if (!_array[hash].Contains(per)) _array[hash].Add(per);
        }
    }

    internal class Person : IEquatable<Person>
    {
        public int Age;
        public string Name;

        public bool Equals(Person other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Age == other.Age && string.Equals(Name, other.Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Person) obj);
        }

        public override int GetHashCode()
        {
            return Age;
        }

        public override string ToString()
        {
            return Age.ToString() + " " + Name;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            HashiTable ht = new HashiTable();
            ht.Add(new Person() {Age = 10, Name = "Yoshy"});
            ht.Add(new Person() { Age = 100, Name = "Owen" });
            ht.Add(new Person() { Age = 101, Name = "Ivan" });
            ht.Add(new Person() { Age = 102, Name = "Pubert" });
        }
    }
}
