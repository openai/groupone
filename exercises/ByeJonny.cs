﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LinqMaraphone
{
    public class DrinkType
    {
        public DrinkType(string name, double degree, double sivuha, int priceAv)
        {
            Name = name;
            Degree = degree;
            Sivuha = sivuha;
            PriceAv = priceAv;
        }

        public string Name { get; }
        public double Degree { get; }
        public double Sivuha { get; }
        public readonly double PriceAv;
    }

    public class Shot
    {
        public DrinkType drinkType { get; set; }
        public double drinkAmount { get; set; }
        public double acquiredPrice { get; set; }
    }

    public class Drinking
    {
        public List<Shot> Shots;
        public DateTime DateTime;
        public bool GotSick;
        public Place Where;
        public override string ToString()
        {
            return Shots.Select(s => s.drinkType.Name).Aggregate("", (s, s1) => s + ", " + s1);
        }
    }

    public class Place
    {
        public Place(string name, bool bodyajutDimedrolom)
        {
            Name = name;
            BodyajutDimedrolom = bodyajutDimedrolom;
        }

        public bool BodyajutDimedrolom { get; }

        public string Name { get; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<DrinkType> drinkTypes = new List<DrinkType>()
            {
                new DrinkType("Beer", 9.0, 50.0 / 100, 10),
                new DrinkType("Vodka", 40.0, 10.0 / 100, 50),
                new DrinkType("Scotch", 45.0, 3400.0 / 100, 200),
                new DrinkType("Wine", 14.0, 600.0 / 100, 120),
                new DrinkType("Cognaq", 45.0, 2000.0 / 100, 300),
                new DrinkType("Poylo№5", 28.0, 8000.0 / 100, 25),
                new DrinkType("Calvados", 40.0, 20000.0 / 100, 700)
            };

            List<Place> places = new List<Place>()
            {
                new Place("Praga Restraunt", true),
                new Place("Berlin Restraunt", true),
                new Place("New-York Restraunt", true),
                new Place("Home", false),
                new Place("Don't remember", false),
            };

            List<Drinking> drinkings = Fillup(places, drinkTypes);

            //Джонни жил-был, пил и умер. Но пил до последнего дня. Когда умер Джонни?
           // var dayOfDeath = drinkings.Max(d => d.DateTime);

            //Сколько же всего литров Джонни выпил за свою жизнь?
           // var wholeAmount = drinkings.Sum(d => d.Shots.Sum(s => s.drinkAmount));

            //Сколько выпил Джонни в пересчете на чистый алкоголь
            //var amountInPureAlcohol = ...

            //Когда Джонни потратил больше всего денег на выпивку?
           // var theMostExpensiveDrinkingDay =
              

            //найти время и мeсто, когда и где Джонни выпил более всего сивушных масел
            //var dayOfSivuha = ...

            //однажды Джонни переплатил за выпивку немерянные деньги. Когда и где?
            //var overpaymentDay = ...

         

            //Однажды Джонни пил пиво, потом вино, потом виски,
            //потом снова вино, потом он пил кальвадос и наконец пойло №5... за сколько дней до смерти это произоходило
            //var dayOfAhtung = 

            //Что Джонни выпил больше всего?
            //var theMost = ...
           //
        }

        private static List<Drinking> Fillup(List<Place> places, List<DrinkType> drinkTypes)
        {
            Random r = new Random(1);

            List<Drinking> result = Enumerable.Range(0, 2000).Select(d =>
                new Drinking()
                {
                    Where = places[r.Next(places.Count)],
                    DateTime = new DateTime(r.Next(1985, 1999), r.Next(1, 13), r.Next(1, 29)),
                    GotSick = true,
                    Shots = GenerateShots(drinkTypes, r)
                }).ToList();
            return result;
        }

        private static List<Shot> GenerateShots(List<DrinkType> drinkTypes, Random r)
        {
            List<Shot> result = Enumerable.Range(0, r.Next(11))
                .Select(s => new Shot()
                {
                    drinkType = drinkTypes[r.Next(drinkTypes.Count)],
                    drinkAmount = r.NextDouble() * r.Next(10)
                }).ToList();
            foreach (var shot in result)
                shot.acquiredPrice = shot.drinkType.PriceAv * 0.5 + r.NextDouble() * 2 * shot.drinkType.PriceAv;

            return result;
        }
    }
}
