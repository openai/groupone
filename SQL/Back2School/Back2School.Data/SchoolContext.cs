﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Back2School.Domain;
using Microsoft.EntityFrameworkCore;

namespace Back2School.Data
{
    public class SchoolContext : DbContext
    {
        public virtual DbSet<Class> Classes { get; set; }
        public virtual DbSet<ClassSubjectSpecialization> ClassSubjectSpecializations { get; set; }
        public virtual DbSet<Classroom> Classrooms { get; set; }
        public virtual DbSet<ClassroomCategory> ClassroomCategories { get; set; }
        public virtual DbSet<Grade> Grades { get; set; }
        public virtual DbSet<Schedule> Schedules { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<StudentAttachment> StudentAttachments { get; set; }
        public virtual DbSet<Subject> Subjects { get; set; }
        public virtual DbSet<SubjectCategory> SubjectCategories { get; set; }
        public virtual DbSet<Teacher> Teachers { get; set; }
        public virtual DbSet<TeacherSubjectSpecialization> TeacherSubjectSpecializations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StudentAttachment>()
                .HasKey(attachment => new {attachment.ClassId, attachment.StudentId});
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=.;Database=School;Trusted_Connection=True;");
        }
    }
}
