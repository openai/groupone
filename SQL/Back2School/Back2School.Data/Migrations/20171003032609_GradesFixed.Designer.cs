﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Back2School.Data;

namespace Back2School.Data.Migrations
{
    [DbContext(typeof(SchoolContext))]
    [Migration("20171003032609_GradesFixed")]
    partial class GradesFixed
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.3")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Back2School.Domain.Class", b =>
                {
                    b.Property<int>("ClassId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreationDate");

                    b.Property<string>("Letter");

                    b.Property<int>("Number");

                    b.HasKey("ClassId");

                    b.ToTable("Classes");
                });

            modelBuilder.Entity("Back2School.Domain.Classroom", b =>
                {
                    b.Property<int>("ClassroomId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ClassroomCategoryId");

                    b.Property<int>("ClassroomNumber");

                    b.HasKey("ClassroomId");

                    b.HasIndex("ClassroomCategoryId");

                    b.ToTable("Classrooms");
                });

            modelBuilder.Entity("Back2School.Domain.ClassroomCategory", b =>
                {
                    b.Property<int>("ClassroomCategoryId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("ClassroomCategoryId");

                    b.ToTable("ClassroomCategories");
                });

            modelBuilder.Entity("Back2School.Domain.ClassSubjectSpecialization", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ClassId");

                    b.Property<int>("SubjectId");

                    b.HasKey("Id");

                    b.HasIndex("ClassId");

                    b.HasIndex("SubjectId");

                    b.ToTable("ClassSubjectSpecializations");
                });

            modelBuilder.Entity("Back2School.Domain.Grade", b =>
                {
                    b.Property<int>("GradeId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("GradeValue");

                    b.Property<int?>("ScheduleId");

                    b.Property<int?>("StudentId");

                    b.HasKey("GradeId");

                    b.HasIndex("ScheduleId");

                    b.HasIndex("StudentId");

                    b.ToTable("Grades");
                });

            modelBuilder.Entity("Back2School.Domain.Schedule", b =>
                {
                    b.Property<int>("ScheduleId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ClassId");

                    b.Property<int?>("ClassroomId");

                    b.Property<DateTime>("Date");

                    b.Property<int>("LessonNum");

                    b.Property<int?>("SubjectId");

                    b.Property<int?>("TeacherId");

                    b.HasKey("ScheduleId");

                    b.HasIndex("ClassId");

                    b.HasIndex("ClassroomId");

                    b.HasIndex("SubjectId");

                    b.HasIndex("TeacherId");

                    b.ToTable("Schedules");
                });

            modelBuilder.Entity("Back2School.Domain.Student", b =>
                {
                    b.Property<int>("StudentId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AdmissionDate");

                    b.Property<DateTime>("BirthDate");

                    b.Property<string>("FirstName");

                    b.Property<int>("KestnerKoeff");

                    b.Property<string>("LastName");

                    b.Property<string>("Patronymic");

                    b.HasKey("StudentId");

                    b.ToTable("Students");
                });

            modelBuilder.Entity("Back2School.Domain.StudentAttachment", b =>
                {
                    b.Property<int>("ClassId");

                    b.Property<int>("StudentId");

                    b.HasKey("ClassId", "StudentId");

                    b.HasIndex("StudentId");

                    b.ToTable("StudentAttachments");
                });

            modelBuilder.Entity("Back2School.Domain.Subject", b =>
                {
                    b.Property<int>("SubjectId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CategorySubjectCategoryId");

                    b.Property<string>("Name");

                    b.HasKey("SubjectId");

                    b.HasIndex("CategorySubjectCategoryId");

                    b.ToTable("Subjects");
                });

            modelBuilder.Entity("Back2School.Domain.SubjectCategory", b =>
                {
                    b.Property<int>("SubjectCategoryId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("SubjectCategoryId");

                    b.ToTable("SubjectCategories");
                });

            modelBuilder.Entity("Back2School.Domain.Teacher", b =>
                {
                    b.Property<int>("TeacherId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("BirthDate");

                    b.Property<int>("EducationLevel");

                    b.Property<string>("FirstName");

                    b.Property<DateTime>("HireDate");

                    b.Property<string>("LastName");

                    b.Property<string>("Patronymic");

                    b.Property<bool>("Race");

                    b.Property<string>("Sex");

                    b.HasKey("TeacherId");

                    b.ToTable("Teachers");
                });

            modelBuilder.Entity("Back2School.Domain.TeacherSubjectSpecialization", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("SubjectId");

                    b.Property<int>("TeacherId");

                    b.HasKey("Id");

                    b.HasIndex("SubjectId");

                    b.HasIndex("TeacherId");

                    b.ToTable("TeacherSubjectSpecializations");
                });

            modelBuilder.Entity("Back2School.Domain.Classroom", b =>
                {
                    b.HasOne("Back2School.Domain.ClassroomCategory", "ClassroomCategory")
                        .WithMany()
                        .HasForeignKey("ClassroomCategoryId");
                });

            modelBuilder.Entity("Back2School.Domain.ClassSubjectSpecialization", b =>
                {
                    b.HasOne("Back2School.Domain.Class", "Class")
                        .WithMany("Specialization")
                        .HasForeignKey("ClassId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Back2School.Domain.Subject", "Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Back2School.Domain.Grade", b =>
                {
                    b.HasOne("Back2School.Domain.Schedule", "Schedule")
                        .WithMany()
                        .HasForeignKey("ScheduleId");

                    b.HasOne("Back2School.Domain.Student", "Student")
                        .WithMany()
                        .HasForeignKey("StudentId");
                });

            modelBuilder.Entity("Back2School.Domain.Schedule", b =>
                {
                    b.HasOne("Back2School.Domain.Class", "Class")
                        .WithMany()
                        .HasForeignKey("ClassId");

                    b.HasOne("Back2School.Domain.Classroom", "Classroom")
                        .WithMany()
                        .HasForeignKey("ClassroomId");

                    b.HasOne("Back2School.Domain.Subject", "Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId");

                    b.HasOne("Back2School.Domain.Teacher", "Teacher")
                        .WithMany()
                        .HasForeignKey("TeacherId");
                });

            modelBuilder.Entity("Back2School.Domain.StudentAttachment", b =>
                {
                    b.HasOne("Back2School.Domain.Class", "Class")
                        .WithMany("StudentAttachments")
                        .HasForeignKey("ClassId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Back2School.Domain.Student", "Student")
                        .WithMany("StudentAttachments")
                        .HasForeignKey("StudentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Back2School.Domain.Subject", b =>
                {
                    b.HasOne("Back2School.Domain.SubjectCategory", "Category")
                        .WithMany()
                        .HasForeignKey("CategorySubjectCategoryId");
                });

            modelBuilder.Entity("Back2School.Domain.TeacherSubjectSpecialization", b =>
                {
                    b.HasOne("Back2School.Domain.Subject", "Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Back2School.Domain.Teacher", "Teacher")
                        .WithMany("Specialization")
                        .HasForeignKey("TeacherId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
