﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Back2School.Data.Migrations
{
    public partial class ManytoManyOut : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_TeacherSubjectSpecializations",
                table: "TeacherSubjectSpecializations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClassSubjectSpecializations",
                table: "ClassSubjectSpecializations");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "TeacherSubjectSpecializations",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "ClassSubjectSpecializations",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_TeacherSubjectSpecializations",
                table: "TeacherSubjectSpecializations",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClassSubjectSpecializations",
                table: "ClassSubjectSpecializations",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherSubjectSpecializations_TeacherId",
                table: "TeacherSubjectSpecializations",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassSubjectSpecializations_ClassId",
                table: "ClassSubjectSpecializations",
                column: "ClassId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_TeacherSubjectSpecializations",
                table: "TeacherSubjectSpecializations");

            migrationBuilder.DropIndex(
                name: "IX_TeacherSubjectSpecializations_TeacherId",
                table: "TeacherSubjectSpecializations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClassSubjectSpecializations",
                table: "ClassSubjectSpecializations");

            migrationBuilder.DropIndex(
                name: "IX_ClassSubjectSpecializations_ClassId",
                table: "ClassSubjectSpecializations");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "TeacherSubjectSpecializations");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "ClassSubjectSpecializations");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TeacherSubjectSpecializations",
                table: "TeacherSubjectSpecializations",
                columns: new[] { "TeacherId", "SubjectId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClassSubjectSpecializations",
                table: "ClassSubjectSpecializations",
                columns: new[] { "ClassId", "SubjectId" });
        }
    }
}
