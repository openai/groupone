﻿namespace Back2School.Domain
{
    public class Grade
    {
        public int GradeId { get; set; }
        public Schedule Schedule { get; set; }
        public Student Student { get; set; }
        public int GradeValue { get; set; }
    }
}