﻿using System;

namespace Back2School.Domain
{
    public class Schedule
    {
        public int ScheduleId { get; set; }
        public DateTime Date { get; set; }
        public int LessonNum { get; set; }
        public Subject Subject { get; set; }
        public Classroom Classroom { get; set; }
        public Class Class { get; set; }
        public Teacher Teacher { get; set; }
    }
}