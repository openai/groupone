﻿namespace Back2School.Domain
{
    public class ClassroomCategory
    {
        public int ClassroomCategoryId { get; set; }
        public string Name { get; set; }
    }
}