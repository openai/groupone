﻿namespace Back2School.Domain
{
    public class ClassSubjectSpecialization
    {
        public int Id { get; set; }
        public int ClassId { get; set; }
        public Class Class { get; set; }
        public int SubjectId { get; set; }
        public Subject Subject { get; set; }
    }
}