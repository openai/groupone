﻿using System;
using System.Collections.Generic;

namespace Back2School.Domain
{
    public class Class
    {
        public int ClassId { get; set; }
        public DateTime CreationDate { get; set; }
        public int Number { get; set; }
        public string Letter { get; set; }
        public virtual ICollection<ClassSubjectSpecialization> Specialization { get; set; }
        public virtual ICollection<StudentAttachment> StudentAttachments { get; set; }
    }
}