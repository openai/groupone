namespace Back2School.Domain
{
    public class Subject
    {
        public int SubjectId { get; set; }
        public string Name { get; set; }
        public SubjectCategory Category { get; set; }
    }
}