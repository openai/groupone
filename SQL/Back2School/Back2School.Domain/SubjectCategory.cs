﻿namespace Back2School.Domain
{
    public class SubjectCategory
    {
        public int SubjectCategoryId { get; set; }
        public string Name { get; set; }
    }
}