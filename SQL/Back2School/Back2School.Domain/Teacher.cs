﻿using System;
using System.Collections.Generic;

namespace Back2School.Domain
{
    public class Teacher
    {
        public int TeacherId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Patronymic { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime HireDate { get; set; }
        public int EducationLevel { get; set; }
        public string Sex { get; set; }
        public bool Race { get; set; }
        public virtual ICollection<TeacherSubjectSpecialization> Specialization { get; set; }
    }
}