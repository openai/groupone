﻿namespace Back2School.Domain
{
    public class Classroom
    {
        public int ClassroomId { get; set; }
        public int ClassroomNumber { get; set; }
        public ClassroomCategory ClassroomCategory { get; set; }
    }
}