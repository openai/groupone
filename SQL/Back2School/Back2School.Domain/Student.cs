﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back2School.Domain
{
    public class Student 
    {
        public int StudentId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Patronymic { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime AdmissionDate { get; set; }
        public int KestnerKoeff { get; set; }
        public virtual ICollection<StudentAttachment> StudentAttachments { get; set; }
    }
}
