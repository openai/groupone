﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ConverterTest
{
    /// <summary>
    /// Interaction logic for MyControlo.xaml
    /// </summary>
    public partial class MyControlo : UserControl
    {
        public MyControlo()
        {
            InitializeComponent();

            Binding binding = new Binding("Value");
            binding.Source = TheScrollBar;
            binding.Converter = new MyValueConverter();
            BindingOperations
                .SetBinding(this, OutputValueProperty, binding);
        }

        public static readonly DependencyProperty OutputValueProperty 
            = DependencyProperty.Register(
            "OutputValue", typeof(double), 
            typeof(MyControlo), new PropertyMetadata(0.05));

        public double OutputValue
        {
            get { return (double) GetValue(OutputValueProperty); }
            set { SetValue(OutputValueProperty, value); }
        }
    }

    public class MyValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var v = value as double?;
            if (v != null)
                return v.Value * 0.01;
            return 0.0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
          throw new NotImplementedException();
        }
    }
}
