﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using AbobeHotoshop.Filters;
using Microsoft.Win32;
using Prism.Commands;
using Prism.Mvvm;

namespace AbobeHotoshop.Main
{
    public class MainViewVM : BindableBase
    {
        private Filter _currentFiter;
        private string _imagePath;
        public List<Filter> Filters { get; private set; }

        public Filter CurrentFiter
        {
            get { return _currentFiter; }
            set { SetProperty(ref _currentFiter, value); }
        }

        public DelegateCommand<Window> OpenImageCommand { get; private set; }

        public DelegateCommand ExitProgramCommand { get; private set; }

        public string ImagePath
        {
            get { return _imagePath; }
            private set { SetProperty(ref _imagePath, value); }
        }

        public MainViewVM()
        {
            Filters = new List<Filter>()
            {
                new GrayScaleFilter(),
                new InverseFilter(),
                new SwapFilter(),
                new WhiteBlackFilter()
            };

            OpenImageCommand = new DelegateCommand<Window>(ownr =>
            {
                var dlg = new OpenFileDialog
                {
                    InitialDirectory = Path.Combine(Environment.CurrentDirectory, "Images")
                };
                if (dlg.ShowDialog(ownr) == true && !string.IsNullOrEmpty(dlg.FileName) )
                {
                    ImagePath = dlg.FileName;
                }
            });

            ExitProgramCommand = new DelegateCommand(() => { Application.Current.Shutdown(0);});
        }
    }
}
