using System;
using System.Windows.Media.Imaging;

namespace AbobeHotoshop.Filters
{
    public class Filter
    {
        public virtual string Name => "������������";

        public virtual void ProcessImage(WriteableBitmap wbitmap, double primaryIntensity, double secondaryIntensity)
        {
            unsafe
            {
                int width = wbitmap.PixelWidth;
                int height = wbitmap.PixelHeight;
                int stride = wbitmap.BackBufferStride;
                int bytesPerPixel = (wbitmap.Format.BitsPerPixel + 7) / 8;

                byte* pImgData = (byte*)wbitmap.BackBuffer;

                
                int cRowStart = 0;
                int cColStart = 0;
                for (int row = 0; row < height; row++)
                {
                    cColStart = cRowStart;
                    for (int col = 0; col < width; col++)
                    {
                        byte* bPixel = pImgData + cColStart;
                        UInt32* iPixel = (UInt32*)bPixel;

                        // invert but maintain alpha
                        //*iPixel = *iPixel ^ 0x00ffffff;
                        //blue component
                        //bPixel[0 /* Bgra */]; 
                        //.. and so on
                        cColStart += bytesPerPixel;
                    }
                    cRowStart += stride;
                }
            }
        }
    }
}