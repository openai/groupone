﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AbobeHotoshop.Filters;

namespace AbobeHotoshop.ImageLabControl
{
    /// <summary>
    /// Interaction logic for ImageLab.xaml
    /// </summary>
    public partial class ImageLab : UserControl
    {
        public ImageLab()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty ImagePathProperty = DependencyProperty.Register(
            "ImagePath", typeof(string), typeof(ImageLab), new PropertyMetadata(default(string), (o, args) =>
            {
                if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) return;
                (o as ImageLab)?.Prog();
                (o as ImageLab)?.Intens();
            }));

        public string ImagePath
        {
            get { return (string) GetValue(ImagePathProperty); }
            set { SetValue(ImagePathProperty, value); }
        }

        public static readonly DependencyProperty PrimaryIntensityProperty = DependencyProperty.Register(
            "PrimaryIntensity", typeof(double), typeof(ImageLab), new PropertyMetadata(default(double), (o, args) =>
            {
                if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) return;
                (o as ImageLab)?.Intens();
            }));

        public double PrimaryIntensity
        {
            get { return (double) GetValue(PrimaryIntensityProperty); }
            set { SetValue(PrimaryIntensityProperty, value); }
        }

        public static readonly DependencyProperty SecondaryIntensityProperty = DependencyProperty.Register(
            "SecondaryIntensity", typeof(double), typeof(ImageLab), new PropertyMetadata(default(double), (o, args) =>
            {
                if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) return;
                (o as ImageLab)?.Intens();
            }));

        public double SecondaryIntensity
        {
            get { return (double) GetValue(SecondaryIntensityProperty); }
            set { SetValue(SecondaryIntensityProperty, value); }
        }

        public static readonly DependencyProperty FilterProperty = DependencyProperty.Register(
            "Filter", typeof(Filter), typeof(ImageLab), new PropertyMetadata(default(Filter), (o, args) =>
            {
                if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) return;
                (o as ImageLab)?.Intens();
            }));

        public Filter Filter
        {
            get { return (Filter) GetValue(FilterProperty); }
            set { SetValue(FilterProperty, value); }
        }

        private BitmapSource _bm;
        void Prog()
        {
            if (String.IsNullOrEmpty(ImagePath)) return;
            // must be compiled with /UNSAFE
            // get an image to draw on and convert it to our chosen format
            _bm = BitmapDecoder.Create(File.Open(ImagePath, FileMode.Open), BitmapCreateOptions.None, BitmapCacheOption.OnLoad)
                .Frames[0];

            if (_bm.Format != PixelFormats.Bgra32)
                _bm = new FormatConvertedBitmap(_bm, PixelFormats.Bgra32, null, 0);
        }

        void Intens()
        {
            if (String.IsNullOrEmpty(ImagePath)) return;

            // get a writable bitmap of that image
            var wbitmap = new WriteableBitmap(_bm);
            wbitmap.Lock();

            if (Filter != null)
                Filter.ProcessImage(wbitmap, PrimaryIntensity, SecondaryIntensity);
            else
                new Filter().ProcessImage(wbitmap, PrimaryIntensity, SecondaryIntensity);
            
            wbitmap.Unlock();
            // if you are going across threads, you will need to additionally freeze the source
            wbitmap.Freeze();

            TheImage.Source = wbitmap;
        }
    }
}
