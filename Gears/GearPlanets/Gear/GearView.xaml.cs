﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GearPlanets.Gear
{
    /// <summary>
    /// Interaction logic for GearView.xaml
    /// </summary>
    public partial class GearView : UserControl
    {
        private Pen _dot;
        public GearView()
        {
            InitializeComponent();
            _dot = new Pen(Brushes.Black, 1.0);
            _dot.Freeze();        
        }

        public static readonly DependencyProperty AngleProperty = DependencyProperty.Register(
            "Angle", typeof(double), typeof(GearView), new PropertyMetadata(default(double), (o, args) =>
            {
                (o as GearView)?.Redraw();
            }));

        public double Angle
        {
            get { return (double) GetValue(AngleProperty); }
            set { SetValue(AngleProperty, value); }
        }

        public static readonly DependencyProperty CountProperty = DependencyProperty.Register(
            "Count", typeof(int), typeof(GearView), new PropertyMetadata(default(int), (o, args) =>
            {
                (o as GearView)?.Redraw();
            }));

        public int Count
        {
            get { return (int) GetValue(CountProperty); }
            set { SetValue(CountProperty, value); }
        }

        public static readonly DependencyProperty ToothWidthProperty = DependencyProperty.Register(
            "ToothWidth", typeof(double), typeof(GearView), new PropertyMetadata(default(double), (o, args) =>
            {
                (o as GearView)?.Redraw();
            }));

        public double ToothWidth
        {
            get { return (double) GetValue(ToothWidthProperty); }
            set { SetValue(ToothWidthProperty, value); }
        }

        private double _margin = 5.0;
        private void Redraw()
        {
            if(ActualHeight == 0 || ActualWidth == 0) return;

            DrawingVisual drawingVisual = new DrawingVisual();
            using (DrawingContext drawingContext = drawingVisual.RenderOpen())
            {
                var xc = ActualWidth / 2.0;
                var yc = ActualHeight / 2.0;
                drawingContext.DrawRoundedRectangle(Brushes.Yellow, _dot,
                    new Rect(new Point(_margin, _margin), new Point(ActualWidth - _margin, ActualHeight - _margin)), 20, 20);

              
            }

            RenderTargetBitmap bmp = new RenderTargetBitmap((int)ActualWidth, (int)ActualHeight, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(drawingVisual);
            // Set the source of the Image control!
            TheImage.Source = bmp;
        }

        private int _curWidth;
        private int _curHeight;
        private void ImageResized(object sender, EventArgs e)
        {
            if (_curWidth == (int)ActualWidth && _curHeight == (int)ActualHeight) return;
            _curWidth = (int)ActualWidth;
            _curHeight = (int)ActualHeight;

            Redraw();
        }
    }
}
