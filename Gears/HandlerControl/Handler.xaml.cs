﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
// ReSharper disable PossibleNullReferenceException

namespace HandlerControl
{
    /// <summary>
    /// Interaction logic for Handler.xaml
    /// </summary>
    public partial class Handler : UserControl
    {
        public Handler()
        {
            InitializeComponent();
            Binding binding = new Binding("Value")
            {
                Converter = new MultiplierConverter(),
                ConverterParameter = new Func<double>(() => Multiplier),
                Source = TheScrollBar,
                Mode = BindingMode.OneWay
            };
            TheControl.SetBinding(OutputValueProperty, binding);
        }
        
        public static readonly DependencyProperty MultiplierProperty = DependencyProperty.Register(
            "Multiplier", typeof(double), typeof(Handler), new PropertyMetadata(default(double)));

        public double Multiplier
        {
            get { return (double) GetValue(MultiplierProperty); }
            set { SetValue(MultiplierProperty, value); }
        }

        public static readonly DependencyProperty OutputValueProperty = DependencyProperty.Register(
            "OutputValue", typeof(double), typeof(Handler), new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsRender));

        public double OutputValue
        {
            get { return (double) GetValue(OutputValueProperty); }
            set { SetValue(OutputValueProperty, value); }
        }

        public static readonly DependencyProperty CaptionProperty = DependencyProperty.Register(
            "Caption", typeof(string), typeof(Handler), new PropertyMetadata(default(string)));

        public string Caption
        {
            get { return (string) GetValue(CaptionProperty); }
            set { SetValue(CaptionProperty, value); }
        }

        public static readonly DependencyProperty MaximumProperty = DependencyProperty.Register(
            "Maximum", typeof(double), typeof(Handler), new FrameworkPropertyMetadata(100.0));

        public double Maximum
        {
            get { return (double) GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }

        public static readonly DependencyProperty MinimumProperty = DependencyProperty.Register(
            "Minimum", typeof(double), typeof(Handler), new FrameworkPropertyMetadata(default(double)));

        public double Minimum
        {
            get { return (double) GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }

        private void Handler_OnLoaded(object sender, RoutedEventArgs e)
        {
            GetBindingExpression(Handler.OutputValueProperty).UpdateTarget();
        }
    }

    public class MultiplierConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var v = value as double? ?? 0.0;
            var func = parameter as Func<double>;
            return v * func?.Invoke() ?? v;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var v = value as double? ?? 0.0;
            var func = parameter as Func<double>;
            return v / func?.Invoke() ?? v;
        }
    }
}
