﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsPrj
{
    class LtdFirmOoo
    {
        public Action SalaryIsReady;

        public void SetSalaryReady()
        {
            SalaryIsReady();
        }
    }

    class Person
    {
        public readonly string Name;

        public Person(string name, LtdFirmOoo firm)
        {
            Name = name;
            firm.SalaryIsReady += GoGetMoney;
        }

        private void GoGetMoney()
        {
            Console.WriteLine($"{Name} сходил и получил салари");
        }
    }

    class EvilPerson
    {
        private readonly string _name;

        public EvilPerson(string name, LtdFirmOoo firm)
        {
            _name = name;
            foreach (var inv in firm.SalaryIsReady.GetInvocationList())
            {
                Console.WriteLine($"{(inv.Target as Person)?.Name} хрен что получит");
            }
            firm.SalaryIsReady = null;
            firm.SalaryIsReady += GoGetMoney;
        } 
    

        private void GoGetMoney()
        {
            Console.WriteLine($"{_name} сходил и получил всю салари");
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            LtdFirmOoo theFirm = new LtdFirmOoo();
            Person a = new Person("A the Person", theFirm);
            Person b = new Person("B the Person", theFirm);
            EvilPerson ep = new EvilPerson("The Evil Itself", theFirm);
            theFirm.SetSalaryReady();

            Console.ReadKey();
        }
    }
}
