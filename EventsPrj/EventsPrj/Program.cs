﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsPrj
{
    class LtdFirmOoo
    {
        public event EventHandler SalaryIsReadyPublic {
            add
            {
                if (!(value.Target is EvilPerson))
                    SalaryIsReadyReal += value;
            }
            remove
            {
                SalaryIsReadyReal -= value;
            }
        }

        private event EventHandler SalaryIsReadyReal;

        public void SetSalaryReady()
        {
            SalaryIsReadyReal?.Invoke(this, new EventArgs());
        }
    }

    class Person
    {
        public readonly string Name;

        public Person(string name, LtdFirmOoo firm)
        {
            Name = name;
            firm.SalaryIsReadyPublic += GoGetMoney;
        }

        private void GoGetMoney(object sender, EventArgs e)
        {
            Console.WriteLine($"{Name} сходил и получил салари");
        }
    }

    class EvilPerson
    {
        private readonly string _name;

        public EvilPerson(string name, LtdFirmOoo firm)
        {
            
            _name = name;
            firm.SalaryIsReadyPublic += GoGetMoney;
        } 
    

        private void GoGetMoney(object sender, EventArgs eventArgs)
        {
            Console.WriteLine($"{_name} сходил и получил всю салари");
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            LtdFirmOoo theFirm = new LtdFirmOoo();
            Person a = new Person("A the Person", theFirm);
            Person b = new Person("B the Person", theFirm);
            EvilPerson ep = new EvilPerson("The Evil Itself", theFirm);
            theFirm.SetSalaryReady();

            Console.ReadKey();
        }
    }
}
