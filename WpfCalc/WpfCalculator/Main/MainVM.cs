﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;

namespace WpfCalculator.Main
{
    public class MainVM : BindableBase
    {
        private int _number1;
        private int _number2;

        public int Number1
        {
            get { return _number1; }
            set { _number1 = value; RaisePropertyChanged(nameof(Summ));}
        }

        public int Number2
        {
            get { return _number2; }
            set { _number2 = value; RaisePropertyChanged(nameof(Summ)); }
        }

        public int Summ => Calculator.CalculatorMachine.Add(Number1, Number2);
    }
}
