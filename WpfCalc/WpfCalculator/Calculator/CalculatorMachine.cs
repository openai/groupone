﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfCalculator.Calculator
{
    public static class CalculatorMachine
    {
        public static int Add(int i1, int i2)
        {
            return i1 + i2;
        }
    }
}
