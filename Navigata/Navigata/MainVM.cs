﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Navigata.Liberia;
using Navigata.Mozambic;
using Prism.Commands;
using Prism.Mvvm;

namespace Navigata
{
    public class MainVM : BindableBase 
    {
        private BindableBase _frame;

        public MainVM()
        {
            LiberiaCommand = new DelegateCommand(() => Frame = new LiberiaVM());
            MozambicCommand = new DelegateCommand(() => Frame = new MozambicVM());
        }
        public DelegateCommand LiberiaCommand { get; private set; }
        public DelegateCommand MozambicCommand { get; private set; }

        public BindableBase Frame
        {
            get { return _frame; }
            private set {SetProperty(ref _frame, value); }
        }
    }
}
