﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListBoxo.Models
{
    public abstract class Product
    {
        public string Name { get; private set; }
        public string Country { get; private set; }

        public Product(string name, string country)
        {
            Name = name;
            Country = country;
        }
    }

    public class Food : Product
    {
        public int Freshness { get; private set; }

        public Food(string name, string country, int freshness) : base(name, country)
        {
            Freshness = freshness;
        }
    }

    public class Chemicals : Product
    {
        public Chemicals(string name, string country, bool legal)
            : base(name, country)
        {
            Legal = legal;
        }
        public bool Legal { get; private set; } 
    }
}
