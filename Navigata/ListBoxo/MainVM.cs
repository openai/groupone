﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ListBoxo.Models;
using Prism.Mvvm;

namespace ListBoxo
{
    public class MainVM : BindableBase
    {
        public List<Product> Products { get; private set; }

        public MainVM()
        {
            Products = new List<Product>()
            {
                new Chemicals("Дизоксохлоратин", "Мьянма", true),
                new Food("Яблоки", "Польша", 30),
                new Food("Бананы", "Белоруссия", 60),
                new Chemicals("Пирит", "Эстония", true),
                new Chemicals("Полипропилен", "Россия", false),
                new Food("Бройлеры", "США", 10),
                new Food("Травы", "Израиль", 90),
                new Chemicals("Фтор", "Китай", true),
            };
        }
    }
}
