﻿namespace TheGame
{
    class Butcher : Unit
    {
        public Butcher()
        {
            Damage = 8;
            Health = 20;
            MyDefaultAction = " отрубает конечность ";
        }

        public override string Name { get; set; } = "Мясник";
        public override string MySymbol { get; } = "Bt";

    }
}