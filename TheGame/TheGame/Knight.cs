﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame
{
    class Knight : Unit
    {
        public Knight()
        {
            MyDefaultAction = " ударил мечом ";
            Damage = 12;
            Health = 50;
        }
        public override string Name { get; set; } = "Рыцарь";
        public override string MySymbol { get; } = "Kt";
    }
}