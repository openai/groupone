﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame
{
    class Murmillo : Unit
    {
        public Murmillo()
        {
            MyDefaultAction = " Throw chain";
            Damage = 1;
        }

        public override string Name { get; set; } = " Мурмиллон";

        public override string MySymbol { get; } = "Mu";
    }
}
