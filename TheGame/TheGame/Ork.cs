﻿namespace TheGame
{
    class Ork : Unit
    {
        public Ork()
        {
            Damage = 100;
            Health = 100;
            MyDefaultAction = " Скушал на завтрак ";
        }

        public override string Name { get; set; } = "Орк";
        public override string MySymbol { get; } = "Ork";

    }
}
