﻿namespace TheGame
{
    class LazyFatGuy : Unit
    {
        public LazyFatGuy()
        {
            Damage = 0;
            Health = 30;
            MyDefaultAction = " ничего не делает с ";
        }

        public override string Name { get; set; } = "Лентяй";
        public override string MySymbol { get; } = "Lz";

    }
}