using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace TheGame
{
    public class GameMaster
    {
        const ConsoleColor OurColors = ConsoleColor.Green;
        const ConsoleColor IhaColors = ConsoleColor.Cyan;
        static readonly Random Rnd = new Random();
        private List<Unit> MyUnits;
        private List<Unit> Enemies; 
        private Phase _phaseState = Phase.ChooseUnit1;
        private ActionType _actionType = ActionType.Attack;

        public GameMaster()
        {
            MyUnits = GenerateUnits(Rnd, 5, OurColors);
            Enemies = GenerateUnits(Rnd, 5, IhaColors);
        }

        private static List<Unit> GenerateUnits(Random r, int count, ConsoleColor color)
        {
            var units = new List<Unit>();
            for (int i = 0; i < count; ++i)
            {
                var prototypes = GetUnitPrototypes();
                //var prototypes = new List<Unit>() {new Peasant()};

                var unit = prototypes[r.Next(prototypes.Count - 1)];
                unit.MyColor = color;
                unit.MyIndex = i;
                units.Add(unit);
            }
            return units;
        }

        public static List<Unit> GetUnitPrototypes() 
        {
            List<Unit> objects = new List<Unit>();
            foreach (Type type in
                Assembly.GetAssembly(typeof(Unit)).GetTypes()
                    .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(Unit))))
            {
                objects.Add((Unit)Activator.CreateInstance(type));
            }
            return objects;
        }

        public void RenderScene()
        {
            Console.Clear();
            DrawUnits(MyUnits, OurColors);
            DrawUnits(Enemies, IhaColors);
            DrawPromptString();
            DrawActionProcess();
        }

        List<ActionString> _actionStrings = new List<ActionString>();

        public void DoAction()
        {
            switch (_phaseState)
            {
                case Phase.ChooseUnit1:
                    _actionStrings.Clear();
                    break;
                case Phase.Action:
                {
                    if (_actionType == ActionType.Attack)
                    {
                        var actionString = _unitFirst.Punch(_unitSecond);
                        _actionStrings.Add(actionString);
                    }
                    else // _actionType == ActionType.Special...
                    {
                        var actionString = _unitFirst.DoSpecial(_unitSecond);
                        _actionStrings.Add(actionString);
                    }
                    var unit1 = AIChooseRandomAliveUnit(Enemies);
                    var unit2 = AIChooseRandomAliveUnit(MyUnits);
                    if (unit1 != null && unit2 != null)
                    {
                        var actionString2 = unit1.Punch(unit2);
                        _actionStrings.Add(actionString2);
                    }
                }
                    break;
                default:
                    break;
                    //throw new ArgumentOutOfRangeException();
            }
        }


        private Unit AIChooseRandomAliveUnit(List<Unit> units)
        {
            Random r = new Random();
            var aliveUnits = units.Where(u => u.IsAlive()).ToList();
            if (!aliveUnits.Any())
                return null;
            var unit = aliveUnits[ r.Next(aliveUnits.Count)];
            return unit;
        }

        private void DrawActionProcess()
        {
            foreach (var actionString in _actionStrings)
                actionString.Render();

        }

        private void DrawPromptString()
        {
            switch (_phaseState)
            {
                case Phase.ChooseUnit1:
                    Console.WriteLine("�������� ���� ����");
                    break;
                    case Phase.ChooseActionType:
                        Console.WriteLine("�������� ��� ��������: 1 - ������� �����, 2 - ������������");
                        break;
                case Phase.ChooseUnit2:
                    if (_actionType == ActionType.Attack || _actionType == ActionType.SpecialOnEnemies)
                        Console.WriteLine("�������� ���� ����������");
                    else
                    {
                        Console.WriteLine("�������� ���� ���� ��� ������������");
                    }
                    break;
                case Phase.Action:
                    Console.WriteLine("������� ��� �����������");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void DrawUnits(List<Unit> units, ConsoleColor color)
        {
            var sb = new StringBuilder();
            for (var i = 0; i < units.Count; i++)
            {
                var unit = units[i];
                sb.Append(unit.Draw(i));
            }
            var backUp = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.WriteLine(sb);
            Console.ForegroundColor = backUp;
        }

        public enum Phase
        {
            ChooseUnit1, 
            ChooseActionType,
            ChooseUnit2,
            Action
        }

        public enum ActionType
        {
            Attack,
            SpecialOnAllyies,
            SpecialOnEnemies
        }

        private Unit _unitFirst;
        private Unit _unitSecond;
        public void Input(ConsoleKeyInfo key)
        {
            switch (_phaseState)
            {
                case Phase.ChooseUnit1:
                    if ("01234".Contains(key.KeyChar))
                    {
                        int i = int.Parse(key.KeyChar.ToString());
                        if (MyUnits[i].IsAlive())
                        {
                            _unitFirst = MyUnits[i];
                            _phaseState = Phase.ChooseActionType;
                        }
                    }
                    break;
                    case Phase.ChooseActionType:
                        if ("12".Contains(key.KeyChar))
                        {
                            int i = int.Parse(key.KeyChar.ToString());
                            if (i == 1) _actionType = ActionType.Attack;
                            else if (i == 2)
                                _actionType =
                                    _unitFirst.IsSpecialOnAllyies()
                                        ? ActionType.SpecialOnAllyies
                                        : ActionType.SpecialOnEnemies;
                            _phaseState = Phase.ChooseUnit2;
                        }
                        break;
                case Phase.ChooseUnit2:
                    if ("01234".Contains(key.KeyChar))
                    {
                        int i = int.Parse(key.KeyChar.ToString());
                        if (_actionType == ActionType.SpecialOnEnemies || _actionType == ActionType.Attack)
                        {
                            if (Enemies[i].IsAlive())
                            {
                                _unitSecond = Enemies[i];
                                _phaseState = Phase.Action;
                            }
                        }
                        else //_actionType == ActionType. onAllyes
                        {
                            if (MyUnits[i].IsAlive())
                            {
                                _unitSecond = MyUnits[i];
                                _phaseState = Phase.Action;
                            }
                        }
                    }
                    break;
                case Phase.Action:
                    _phaseState = Phase.ChooseUnit1;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public bool IsOver()
        {
            return !(MyUnits.Any(u => u.IsAlive()) && Enemies.Any(u => u.IsAlive()));
        }
    }
}