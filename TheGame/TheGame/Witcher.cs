﻿namespace TheGame
{
    class Witcher : Unit
    {
        public Witcher()
        {
            MyDefaultAction = " ё*нул молнией ";
            Damage = 5;
        }
        public override string Name { get; set; } = "Ведьман";
        public override string MySymbol { get; } = "Wt";
    }
}