﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame
{
    class Rogue : Unit
    {
        public Rogue()
        {
            Damage = 20;
            Health = 12;
            MyDefaultAction = " удар сзади ";
        }

        public override string Name { get; set; } = "Убийца";
        public override string MySymbol { get; } = "Rg";
    }
}
