namespace TheGame
{
    class Peasant : Unit
    {
        public Peasant()
        {
            Damage = 1;
            MyDefaultAction = " ������� ������� �� ";
            MySpecialAction = " ������� ����� � �������� ";
        }
        public override string Name { get; set; } = "����������";
        public override string MySymbol { get; } = "Ps";
        public override bool IsSpecialOnAllyies() => true;
        public override ActionString DoSpecial(Unit objectUnit)
        {
            var result = new ActionString();
            result.Subject = MyIndex + " " + Name;
            result.SubjectColor = MyColor;

            objectUnit.Health += 10;
            
            result.Action = MySpecialAction;

            result.Object = objectUnit.MyIndex + " " + objectUnit.Name;
            result.ObjectColor = objectUnit.MyColor;

            result.Damage = -10;

            return result;
        }
    }
}