﻿namespace TheGame
{
    class Archer : Unit
    {
        public Archer()
        {
            MyDefaultAction = " ударил арбалетом ";
            Damage = 5;
        }
        public override string Name { get; set; } = "Лучник";
        public override string MySymbol { get; } = "Ar";
    }
}