﻿using System;

namespace TheGame
{
    public abstract class Unit
    {
        public abstract string Name { get; set; }
        public abstract string MySymbol { get; }
        public int Health = 10;
        protected int Damage = 2;
        public ConsoleColor MyColor;
        public int MyIndex;

        public virtual string Draw(int i)
        {
            return $"[ {i} {MySymbol} {Health}]";
        }
        
        public bool IsAlive()
        {
            return Health > 0;
        }

        public ActionString Punch(Unit objectUnit)
        {
            var result = new ActionString();
            result.Subject = MyIndex + " " + Name;
            result.SubjectColor = MyColor;
            
            objectUnit.Health -= Damage;
            if (objectUnit.Health <= 0)
                result.Action = " убил ";
            else
                result.Action = MyDefaultAction;

            result.Object = objectUnit.MyIndex + " " + objectUnit.Name;
            result.ObjectColor = objectUnit.MyColor;

            result.Damage = Damage;

            return result;
        }
        
        public virtual bool IsSpecialOnAllyies() => false;

        public string MyDefaultAction { get; protected set; }

        public string MySpecialAction { get; protected set; }

        public virtual ActionString DoSpecial(Unit objectUnit)
        {
            var result = new ActionString();
            result.Subject = MyIndex + " " + Name;
            result.SubjectColor = MyColor;

            objectUnit.Health -= Damage;
            if (objectUnit.Health <= 0)
                result.Action = " убил ";
            else
                result.Action = MyDefaultAction;

            result.Object = objectUnit.MyIndex + " " + objectUnit.Name;
            result.ObjectColor = objectUnit.MyColor;

            result.Damage = Damage;

            return result;
        }
       
    }
    
    public class ActionString
    {
        public string Subject;
        public string Action;
        public string Object;
        public ConsoleColor SubjectColor;
        public ConsoleColor ObjectColor;
        public int Damage;

        public void Render()
        {
            var backUp = Console.ForegroundColor;
            Console.ForegroundColor = SubjectColor;
            Console.Write(Subject + " ");

            Console.ForegroundColor = backUp;
            Console.Write(Action + " ");

            Console.ForegroundColor = ObjectColor;
            Console.Write(Object);

            Console.ForegroundColor = backUp;
            Console.WriteLine(" на " + Damage + " урона");
        }
    }
}