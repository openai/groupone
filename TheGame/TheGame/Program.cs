﻿using System;
using System.Threading.Tasks;

namespace TheGame
{
    class Program
    {
        static void Main(string[] args)
        { 
            var gm = new GameMaster();

            while (!gm.IsOver())
            {
                gm.RenderScene();
                gm.Input(Console.ReadKey());
                gm.DoAction();
            }
            Console.WriteLine("Game is over");
            Console.ReadLine();
        }
    }
}
