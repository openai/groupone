﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfCustomSquare
{
    /// <summary>
    /// Interaction logic for SquareGraphics.xaml
    /// </summary>
    public partial class SquareGraphics : UserControl
    {
        private int _curWidth;
        private int _curHeight;
        public SquareGraphics()
        {
            InitializeComponent();
        }

    public static readonly DependencyProperty 
            AProperty = DependencyProperty.Register(
            "A", typeof(int), typeof(SquareGraphics),
            new PropertyMetadata(default(int), (s, a) =>
            {
                (s as SquareGraphics)?.Redraw();
            } ));

        public int A
        {
            get { return (int) GetValue(AProperty); }
            set { SetValue(AProperty, value); }
        }

        public static readonly DependencyProperty BProperty = DependencyProperty.Register(
            "B", typeof(int), typeof(SquareGraphics), new PropertyMetadata(default(int)));

        public int B
        {
            get { return (int) GetValue(BProperty); }
            set { SetValue(BProperty, value); }
        }

        public static readonly DependencyProperty CProperty = DependencyProperty.Register(
            "C", typeof(int), typeof(SquareGraphics), new PropertyMetadata(default(int)));

        public int C
        {
            get { return (int) GetValue(CProperty); }
            set { SetValue(CProperty, value); }
        }

        private void ImageResized(object sender, EventArgs e)
        {
            if(_curWidth == (int)ActualWidth 
                && _curHeight == (int)ActualHeight)
                return;
            _curWidth = (int)ActualWidth;
            _curHeight = (int)ActualHeight;

            Redraw();
        }

        private void Redraw()
        {
            DrawingVisual dv = new DrawingVisual();
            DrawingContext context = dv.RenderOpen();

            var text = new FormattedText(A.ToString(),
                CultureInfo.InvariantCulture,
                FlowDirection.LeftToRight,
                new Typeface(this.FontFamily, this.FontStyle,
                    this.FontWeight, this.FontStretch), 24, Brushes.Black);

            context.DrawRoundedRectangle(Brushes.Yellow,
                new Pen(Brushes.Black, 2),
                new Rect(new Point(5, 5),
                    new Point(ActualWidth - 10, ActualHeight - 10)),
                5, 5);
            context.DrawText(text,
                new Point(ActualWidth / 2, ActualHeight / 2));
            for(int i = 0; i < A; ++i)
                context.DrawRectangle(Brushes.Black, 
                    new Pen(Brushes.Black, 1),
                    new Rect(new Point(10 + i, 10 + i), new Size(0,0))
                    );


            context.Close();

            RenderTargetBitmap bitmap = new RenderTargetBitmap(
                ((int)ActualWidth) == 0 ? 1 : ((int)ActualWidth),
                (int)ActualHeight == 0 ? 1 : (int)ActualHeight,
                96, 96, PixelFormats.Pbgra32);
            bitmap.Render(dv);

            TheImage.Source = bitmap;
        }
    }
}
 