﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOTest
{
    public class MainViewVM
    {
        public List<Product> Products { get; set; }
        public MainViewVM()
        {
            Products = new List<Product>();
            using (SqlConnection connection
                = new SqlConnection(
                    "Server=.;Database=NorthWnd;Trusted_Connection=True;"))
            {
                connection.Open();

                SqlCommand getProducts = new SqlCommand(
                    @"SELECT [ProductName]
                        ,[UnitPrice]
                    ,[UnitsInStock]
                FROM[NORTHWND].[dbo].[Products]", connection);

                using (SqlDataReader reader = getProducts.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var product = new Product();
                        product.Name = (string) reader["ProductName"];
                        product.Count = (short)reader["UnitsInStock"];
                        product.Price = (decimal)reader["UnitPrice"];
                         
                        Products.Add(product);
                    }
                }

                connection.Close();
            }
        }
    }

    public class Product
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Count { get; set; }
    }
}
