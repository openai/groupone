﻿using Prism.Mvvm;

namespace MachineModel
{
    public class MoneyStack : BindableBase
    {
        private Money _money;
        private int _count;

        public MoneyStack(Money money, int count)
        {
            _money = money;
            Count = count;
        }

        public Money Money => _money;

        public int Count
        {
            get { return _count; }
            private set { SetProperty(ref _count, value); }
        }

        internal void RemoveCoin()
        {
            if (Count > 0) --Count;
        }

        internal void AddCoin()
        {
            ++Count;
        }
    }
}