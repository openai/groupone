﻿using System.Collections.Generic;

namespace MachineModel
{
    public class Money
    {
        private Money() { }

        static Money()
        {
            MoneyNominals = new List<Money>()
            {
                new Money() { Name = "Рубль", Nominal = 1, IsBanknote = false},
                new Money() { Name = "2 рубля", Nominal = 2, IsBanknote = false},
                new Money() { Name = "5 рублей", Nominal = 5, IsBanknote = false},
                new Money() { Name = "10 рублей",  Nominal = 10, IsBanknote = false}, 
                new Money() { Name = "50 рублей", Nominal = 50, IsBanknote = true},
                new Money() { Name = "100 рублей", Nominal = 100, IsBanknote = true},
                new Money() { Name = "500 рублей", Nominal = 500, IsBanknote = true},
                new Money() { Name = "1000 рублей", Nominal = 1000, IsBanknote = true},
                new Money() { Name = "5000 рублей", Nominal = 5000, IsBanknote = true},
            };
        }

        public static IReadOnlyList<Money> MoneyNominals { get; }
        public string Name { get; private set; }
        public int Nominal { get; private set; }
        public bool IsBanknote { get; private set; }
    }
}