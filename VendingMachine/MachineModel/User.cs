﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Prism.Mvvm;

namespace MachineModel
{
    public class User : BindableBase
    {
        public User()
        {
            _moneyStack = 
                Money.MoneyNominals.Select(e => new MoneyStack(e, 3)).ToList();
            _cart = new ObservableCollection<ProductStack>();
            Cart = new ReadOnlyObservableCollection<ProductStack>(_cart);
        }

        private readonly List<MoneyStack> _moneyStack;
        public IReadOnlyList<MoneyStack> MoneyStacks => _moneyStack;
        public ReadOnlyObservableCollection<ProductStack> Cart { get; set; }
        private ObservableCollection<ProductStack> _cart;

        public int MoneySum
        {
            get
            {
                return _moneyStack.Sum(e => e.Money.Nominal * e.Count);                
            }
        }

        internal bool GetMoney(Money coin)
        {
            var cs = _moneyStack.First(e => e.Money == coin);
            if (cs.Count == 0) return false;
            cs.RemoveCoin();
            RaisePropertyChanged(nameof(MoneySum));
            return true;
        }

        public void Provide(Product product)
        {
            if (_cart.Any(p => p.Product == product))
                _cart.First(p => p.Product == product).Increase();
            else _cart.Add(new ProductStack(product, 1));

        }
    }
}