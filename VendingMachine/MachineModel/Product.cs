﻿using System.Collections.Generic;

namespace MachineModel
{
    public class Product
    {
        private Product()
        {
        }

        static Product()
        {
            Products = new List<Product>()
            {
                new Product() { Name = "Кофе Баклажан", Price = 10 },
                new Product() { Name = "Кофе Американо", Price = 20 },
                new Product() { Name = "Кофе Воронеж", Price = 50 },
                new Product() { Name = "Чай", Price = 14 },
                new Product() { Name = "Вода", Price = 3 },
                new Product() { Name = "Unknown Liquid", Price = 17 },
            };
        }

        public static IReadOnlyList<Product> Products { get; }

        public string Name { get; private set; }
        public int Price { get; private set; }
    }
}