﻿using Prism.Mvvm;

namespace MachineModel
{
    public class ProductStack : BindableBase
    {
        private int _count;

        public ProductStack(Product product, int count)
        {
            Product = product;
            _count = count;
        }
        public Product Product { get; }

        public int Count
        {
            get { return _count; }
            private set { SetProperty(ref _count, value); }
        }

        internal void Increase()
        {
            ++Count;
        }
    }
}