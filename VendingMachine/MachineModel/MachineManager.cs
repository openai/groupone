namespace MachineModel
{
    public class MachineManager
    {
        public User User { get; }
        public Machine Machine { get; }
        
        public MachineManager()
        {
            User = new User();
            Machine = new Machine();
        }

        public void InsertCoin(Money coin)
        {
            if(User.GetMoney(coin)) Machine.PutMoney(coin);
        }

        public void BuyProduct(Product product)
        {
            if (Machine.BuyProduct(product))
                User.Provide(product);
        }
    }
}