﻿using System.Collections.Generic;
using System.Linq;
using Prism.Mvvm;

namespace MachineModel
{
    public class Machine : BindableBase
    {
        public Machine()
        {
            _moneyStack =
                Money.MoneyNominals.Select(e => new MoneyStack(e, 100)).ToList();
        }

        private readonly List<MoneyStack> _moneyStack;
        private int _credit;
        public IReadOnlyList<MoneyStack> MoneyStacks => _moneyStack;

        public int Credit
        {
            get { return _credit; }
            set { SetProperty(ref _credit, value); }
        }

        public void PutMoney(Money coin)
        {
            _moneyStack.First(m => m.Money == coin).AddCoin();
            Credit+=coin.Nominal;
        }

        internal bool BuyProduct(Product product)
        {
            if (Credit < product.Price) return false;
            Credit -= product.Price;
            return true;
        }
    }
}