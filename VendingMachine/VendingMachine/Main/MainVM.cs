﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MachineModel;
using Prism.Commands;
using Prism.Mvvm;

namespace VendingMachine.Main
{
    public class MainVM : BindableBase
    {
        private MachineManager _manager;
        
        public MainVM()
        {
            _manager = new MachineManager();
            _manager.Machine.PropertyChanged += (s, e)
                => { RaisePropertyChanged(e.PropertyName); };
            _manager.User.PropertyChanged += (s, e)
                => { RaisePropertyChanged(e.PropertyName); };

            UserWallet =
                _manager.User.MoneyStacks.Select(e => new MoneyViewModel(e, _manager)).ToList();
            MachineWallet =
                _manager.Machine.MoneyStacks.Select(e => new MoneyViewModel(e, _manager)).ToList();

            BuyProductCommand = new DelegateCommand<Product>(p => 
            _manager.BuyProduct(p));

        }
        public IReadOnlyList<MoneyViewModel> UserWallet { get; }
        public IReadOnlyList<MoneyViewModel> MachineWallet { get; }

        public ReadOnlyObservableCollection<ProductStack> UserCart => _manager.User.Cart;
        public IReadOnlyList<Product> AvailableProducts => Product.Products;

        public int MoneySum => _manager.User.MoneySum;
        public DelegateCommand<Product> BuyProductCommand { get; }
        public int Credit => _manager.Machine.Credit;
    }

    public class MoneyViewModel : BindableBase
    {
        private MoneyStack _moneyStack;
        private readonly MachineManager _manager;

        public MoneyViewModel(MoneyStack moneyStack, MachineManager manager)
        {
            this._moneyStack = moneyStack;
            _moneyStack.PropertyChanged += (s, e) 
                => RaisePropertyChanged(e.PropertyName);
            _manager = manager;
            InsertMoney = new DelegateCommand(() =>
            {
                _manager.InsertCoin(_moneyStack.Money);
            });
        }

        public string MoneyName => _moneyStack.Money.Name;

        public string MoneyImage => _moneyStack.Money.IsBanknote ? "../Images/banknote.png" : "../Images/coin.png";

        public int Count => _moneyStack.Count;
        public DelegateCommand InsertMoney { get; }
    }
}
