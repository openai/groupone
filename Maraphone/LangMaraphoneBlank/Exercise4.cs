﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangMaraphone
{
    public class Exercise4 : Exercise
    {
        public List<int> result1 = new List<int>(1000000);
        public List<int> result2 = new List<int>(1000000);
        public List<int> result3 = new List<int>(1000000);
        public List<int> result4 = new List<int>(1000000);
        public List<int> result5 = new List<int>(1000000);
        public List<double> result6 = new List<double>(1000000);

        public override void Execute()
        {
            if (Done) return;

            Random r = new Random();

            //4.1 
            //заполните result1 миллионом случайных числел от 0 до 100 включительно =>

            //4.2
            //заполните result2 миллионом случайных числел от 50 до 100 включительно =>

            //4.3
            //заполните result3 миллионом случайных числел от -100 до 100 включительно =>

            //4.4
            //заполните result4 миллионом случайных числел в интервале (-10, 6]-(16, 100] =>

            //4.5
            //заполните result5 ровно теми же случайными числами, что и result4, не используя копирование =>

            //4.6
            //заполните result6 миллионом чисел с плавающей точкой, удовлетворяющих условию [-10, 6]-[16, 100] =>


            /* Место для жалоб             
             * 
             */
             
            //вернитесь в метод Main и теперь 
            //вызывайте конструктор этого класса с аргументом true, так как вы выполнили задание =>
        }

        public Exercise4(bool done) : base(done)
        {
        }
    }
}
