using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangMaraphone
{
    public class Exercise8 : Exercise
    {
        public override void Execute()
        {
            if (Done) return;

            //8.1 ���������� ����� string MyIntToString(int i),
            //������� ����������� ����� � ������ =>

            //8.2 ���������� ����� string MyDoubleToString(double d),
            //������� ����������� ����� � ��������� ������ � ������, � ��������� �� �������� ����� ����� �������.
            // �������� 8.32165498 ������������� � "8.321" =>

            //8.3 ���������� ����� string MyDoubleToPercentString(double d),
            //������� ����������� ����� � ��������� ������ � ��������, � ��������� �� ������� ����� ����� �������.
            // �������� 0.123576 ������������� � "12.35 %" =>

            //8.4 ���������� ����� string MyIntToPreString(int i),
            //������� ����������� ����� ����� � ������ � ���������� ��� ������ �� 7 ������.
            // �������� 123 ������������� � "0000123" =>

            //8.5 ���������� ����� int MyStringToInt(string i),
            //������� ����������� ������ � ����� ����� � � ������ ������� ����������� ���������� FormatException =>

            //8.6 ���������� ����� int MyStringToInt(string i, out bool success),
            //������� ����������� ������ � ����� ����� � � ������ ������� ������������� success � false, � �������� =>
            


            /* ����� ��� �����             
             * 
             */

            //��������� � ����� Main � ������ 
            //��������� ����������� ����� ������ � ���������� true, ��� ��� �� ��������� ������� =>
        }

        public string MyIntToString(int i)
        {
            throw new NotImplementedException();
        }

        public string MyDoubleToString(double d)
        {
            throw new NotImplementedException();
        }

        public string MyDoubleToPercentString(double d)
        {
            throw new NotImplementedException();
        }

        public string MyIntToPreString(int i)
        {
            throw new NotImplementedException();
        }

        public int MyStringToInt(string i)
        {
            throw new NotImplementedException();
        }

        public int MyStringToInt(string i, out bool success)
        {
            throw new NotImplementedException();
        }

        public Exercise8(bool done) : base(done)
        {
        }
    }
}
