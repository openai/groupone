using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangMaraphone
{
    public class Exercise2 : Exercise
    {
        public int result1 = 0;
        public int expected1 = 0;
        public int result2 = 0;
        public int expected2 = 0;
        public double result3 = 0;
        public double expected3 = 0;
        public int expected10 = 0;
        public double expected11 = 0;
        public int result4 = 0;
        public int expected4 = 0;
        public double result5 = 0;
        public int result6 = 0;
        public double result7 = 0;
        public double result8 = 0;
        public double result9 = 0;
        public int result10 = 0;
        public double result11 = 0;
        public int result12 = 0;
        public bool match1 = false;
        public bool match2 = false;
        public bool match3 = false;

        public override void Execute()
        {
            if (Done) return;

            //2.1 
            // �������� - ��� ������!
            int i1 = 6;
            int i2 = 3;
            // �������� i1 �� i2. 
            // ���������, ��� ������ ����������. ��������� �������� � expected1.
            // ��������� �������� � result1
            // �������� �� �������� � ���������� �������� � match1 (�������� ������ true ��� false) => 
            expected1 = 2;
            result1 = i1 / i2;
            match1 = true;

            //2.2
            int i3 = 29;
            int i4 = 10;
            // �������� i3 �� i4. 
            // ���������, ��� ������ ����������. ��������� �������� � expected2.
            // ��������� �������� � result2 
            // �������� �� �������� � ���������� �������� � match2 (�������� ������ true ��� false) =>  
            //expected2 = 
            //result2 = 
            //match2 = 

            double d1 = 29;
            double d2 = 10;
            //2.3
            // �������� d1 �� d2. 
            // ���������, ��� ������ ����������. ��������� �������� � expected3.
            // ��������� �������� � result3
            // �������� �� �������� � ���������� �������� � match3 (�������� ������ true ��� false) => 
            //expected3 = 
            //result3 = 
            //match3 = 

            //2.4
            int i5 = 9;
            int i6 = 10;
            // �������� i5 �� i6. 
            // ���������, ��� ������ ����������. ��������� �������� � expected4.
            // ��������� �������� � result4 => 
            //expected4 = 
            //result4 = 
            
            //2.5
            // �������� d1 �� d2 � ��������� ����������, ����� �������� 3
            // ��������� �������� � result5
            //result5 = 
            
            //2.6
            // �������� i5 �� i6 � ��������� ����������, ����� �������� 1 
            // ��������� �������� � result6 => 
            // result6 = 

            double d7 = 2;
            //2.7
            // ��������� d7 � �������
            // ��������� �������� � result7 => 
            //result7 = 

            double d8 = 2;
            //2.8
            // ��������� d8 � ������� �������
            // ��������� �������� � result8 => 
            //result8 = 

            double d9 = 2;
            //2.9
            // ��������� d9 � ������� ����� ��
            // ��������� �������� � result9 => 
            //result9 = 

            int i10 = 99;
            //2.10
            // ������� ������� �� ������� i10 �� 100
            // ���������, ��� ������ ����������. ��������� �������� � expected10.
            // ��������� �������� � result10 => 
            //expected10 = 
            //result10 =
            
            double d11 = 99;
            //2.11
            // ������� ������� �� ������� d11 �� 100
            // ���������, ��� ������ ����������. ��������� �������� � expected11.
            // ��������� �������� � result11 => 
            //expected11 =
            //result11 =

            /* ����� ��� �����             
             * 
             */

            //��������� � ����� Main � ������ 
            //��������� ����������� ����� ������ � ���������� true, ��� ��� �� ��������� ������� =>
        }

        public Exercise2(bool done) : base(done)
        {
        }
    }
}
