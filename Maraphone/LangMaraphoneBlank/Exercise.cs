﻿namespace LangMaraphone
{
    public abstract class Exercise
    {
        public bool Done;

        protected Exercise(bool done)
        {
            Done = done;
        }

        public abstract void Execute();
    }
}