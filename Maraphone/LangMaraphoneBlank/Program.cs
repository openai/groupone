﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangMaraphone
{
    class Program
    {
        static void Main(string[] args)
        {
            var exercises = new List<Exercise>()
            {
                new Exercise1(false), //типы данных 
                new Exercise2(false), //операции над числами
                new Exercise3(false), //decimal and double
                new Exercise4(false), //Random
                new Exercise5(false), //тернарный оператор
                new Exercise6(false), //выводимые типы
                new Exercise7(false), //работа с консолью
                new Exercise8(false), //парсинг и toString
                new Exercise9(false), //if..else и switch
                new Exercise10(false), //циклы
            };

            exercises.Where(e => !e.Done).ToList().ForEach(e => e.Execute());
        }
    }
}
