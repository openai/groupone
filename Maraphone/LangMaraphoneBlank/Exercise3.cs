﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangMaraphone
{
    public class Exercise3 : Exercise
    {
        public float result1 = 0;
        public float expected1 = 0;
        public float expected2 = 0;
        public float expected2_1 = 0;
        public float expected2_2 = 0;
        public double result2 = 0;
        public decimal expected3 = 0;
        public decimal result3 = 0;
        public decimal expected4 = 0;
        public decimal expected4_1 = 0;
        public decimal result4 = 0;
        public bool match1 = false;
        public bool match2 = false;
        public bool match2_2 = false;
        public bool match3 = false;
        public bool match4 = false;

        public override void Execute()
        {
            if (Done) return;

            //3.1 
            float f1 = 1;
            float f2 = 10;
            // поделите f1 на f2. Результат присвойте oneTenth. 
            // Подумайте, что должно получиться. => 
            float oneTenth = f1 / f2;

            //теперь умножньте oneTenth на f2, отнимите у получившегося f1 и результат присвойте result1 
            //подумайте, что должно получиться. Ожидаемое запишите в expected1 
            // совпадет ли ожидание и реальность запишите в match1 (запишите литеру true или false) => 
            
            //3.2
            double d2 = 0.1;
            double d3 = d2 + d2 + d2 + d2 + d2 + d2 + d2 + d2 + d2 + d2;
            // подсчитайте количество слагаемых d2 в сумме d3
            //подумайте, что должно получиться. Ожидаемое запишите в expected2 
            // совпадет ли ожидание и реальность запишите в match2 (запишите литеру true или false) => 
            
            //вычтите d3 из единицы. ПОдумайте, что должно получиться. Ожидаемое запишите в expected2_1
            //Получившееся умножьте на десять квадриллионов и присвойте результат в result2
            //ПОдумайте, что должно получиться. Ожидаемое запишите в expected2_2
            // совпадет ли ожидание и реальность запишите в match2_2 (запишите литеру true или false) => 
            
            //3.3
            decimal dec3 = 1;
            // поделите dec3 на 10. Результат присвойте oneTenthDec. 
            // Подумайте, что должно получиться. => 
            
            //теперь умножньте oneTenthDec на 10, отнимите у получившегося единицу и результат присвойте result3 
            //подумайте, что должно получиться. Ожидаемое запишите в expected3 
            // совпадет ли ожидание и реальность запишите в match3 (запишите литеру true или false) => 
            
            //3.4
            decimal dec4 = new decimal(0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1);
            // подсчитайте количество слагаемых 0.1 в сумме dec4
            //подумайте, что должно получиться. Ожидаемое запишите в expected4 =>
            
            //вычтите dec4 из единицы. ПОдумайте, что должно получиться. Ожидаемое запишите в expected4_1
            //Получившееся умножьте на охренелион в сиксилярдной степени и присвойте результат в result4
            // совпадет ли ожидание и реальность запишите в match4 (запишите литеру true или false) => 
            
            /* Место для жалоб             
             * 
             */


            //вернитесь в метод Main и теперь 
            //вызывайте конструктор этого класса с аргументом true, так как вы выполнили задание =>
        }

        public Exercise3(bool done) : base(done)
        {
        }
    }
}
