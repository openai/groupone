using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangMaraphone
{
    public class Exercise5 : Exercise
    {
        public int result1 = 0;
        public int result2 = 0;
        public override void Execute()
        {
            if (Done) return;
            
            //5.1 
            // ��������� ��������� �������� �������� �������� result1 ������ 1, ���� i1 ������ 143, ����� 2 => 
            int i1 = 11 * 13;
            
            //5.2
            // ��������� ��������� �������� �������� �������� result2 ������ 1, ���� i1 ������ 143,
            // 2, ���� i1 ������ 143, � 3, ���� ����� 143 => 
            
            //5.3 
            //��������� ������ ��������� ��������, ��������������� ������� int Func3(bool p1, bool p2), ������������ 
            // 0, ���� ��� ��������� ����� false
            // 1, ��� p1 == false � p2 == true
            // 2, ��� p1 == true � p2 == false
            // 3, ��� p1 == true � p2 == true => 

            //5.4
            //��������� ������ ��������� ��������, ��������������� ������� 
            //StrangeAnimal Func4(bool isRed, bool isOne, bool isHorn), ������������ ��������������� ��������.
            //�������� ��� Func4(true, false, true) ������� �������� ������� , 
            //� ��� Func4(false, true, false) ������� ������ ����������. => 

            /* ����� ��� �����             
             * 
             */

            //��������� � ����� Main � ������ 
            //��������� ����������� ����� ������ � ���������� true, ��� ��� �� ��������� ������� =>
        }

        public static int Func3(bool p1, bool p2)
        {
            throw new NotImplementedException();
        }
        
        public enum StrangeAnimal
        {
            RedOneHorn,
            BlueOneHorn,
            RedTwoHorn,
            BlueTwoHorn,
            RedOneTail,
            BlueOneTail,
            RedTwoTail,
            BlueTwoTail
        }

        public static StrangeAnimal Func4(bool isRed, bool isOne, bool isHorn)
        {
            throw new NotImplementedException();
        }

        public Exercise5(bool done) : base(done)
        {
        }
    }
}
