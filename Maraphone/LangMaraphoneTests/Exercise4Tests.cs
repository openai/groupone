﻿using System.Linq;
using LangMaraphone;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LangMaraphoneTests
{
    [TestClass()]
    public class Exercise4Tests
    {
        public Exercise4 GetTestObj()
        {
            var ret = new Exercise4(false);
            ret.Execute();
            return ret;
        }

        [TestMethod()]
        public void ExecuteTest1()
        {
            var ob = GetTestObj();
            var list = ob.result1;
            Assert.AreEqual(1000000, list.Count);
            Assert.AreEqual(0, list.Min());
            Assert.AreEqual(100, list.Max());
        }

        [TestMethod()]
        public void ExecuteTest2()
        {
            var ob = GetTestObj();
            var list = ob.result2;
            Assert.AreEqual(1000000, list.Count);
            Assert.AreEqual(50, list.Min());
            Assert.AreEqual(100, list.Max());
        }

        [TestMethod()]
        public void ExecuteTest3()
        {
            var ob = GetTestObj();
            var list = ob.result3;
            Assert.AreEqual(1000000, list.Count);
            Assert.AreEqual(-100, list.Min());
            Assert.AreEqual(100, list.Max());
        }

        [TestMethod()]
        public void ExecuteTest4()
        {
            var ob = GetTestObj();
            var list = ob.result4;
            Assert.AreEqual(1000000, list.Count);
            Assert.AreEqual(-9, list.Min());
            Assert.AreEqual(100, list.Max());
            Assert.AreEqual(false, list.Any(e => e > 6 && e <= 16));
        }

        [TestMethod()]
        public void ExecuteTest5()
        {
            var ob = GetTestObj();
            var list1 = ob.result4;
            var list2 = ob.result5;
            Assert.AreEqual(true, list1.SequenceEqual(list2));
        }

        [TestMethod()]
        public void ExecuteTest6()
        {
            var ob = GetTestObj();
            var list = ob.result6;
            Assert.AreEqual(1000000, list.Count);
            Assert.AreEqual(false, list.Any(e=>e < -10));
            Assert.AreEqual(false, list.Any(e => e > 100));
            Assert.AreEqual(false, list.Any(e => e > 6 && e < 16));
            Assert.AreEqual(true, list.Any(e => e > -10 && e < -9));
            Assert.AreEqual(true, list.Any(e => e > 99 && e < 100));
        }
    }
}