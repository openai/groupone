﻿using LangMaraphone;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LangMaraphoneTests
{
    [TestClass()]
    public class Exercise2Tests
    {
        public Exercise2 GetTestObj()
        {
            var ret = new Exercise2(false);
            ret.Execute();
            return ret;
        }

        [TestMethod()]
        public void ExecuteTest1()
        {
            var ob = GetTestObj();

            Assert.AreEqual(2, ob.expected1);
            Assert.AreEqual(2, ob.result1);
            Assert.AreEqual(true, ob.match1);
        }

        [TestMethod()]
        public void ExecuteTest2()
        {
            var ob = GetTestObj();

            Assert.AreEqual(2, ob.expected2);
            Assert.AreEqual(2, ob.result2);
            Assert.AreEqual(true, ob.match2);
        }

        [TestMethod()]
        public void ExecuteTest3()
        {
            var ob = GetTestObj();

            Assert.AreEqual(2.9, ob.expected3);
            Assert.AreEqual(2.9, ob.result3);
            Assert.AreEqual(true, ob.match3);
        }

        [TestMethod()]
        public void ExecuteTest4()
        {
            var ob = GetTestObj();

            Assert.AreEqual(0, ob.expected4);
            Assert.AreEqual(0, ob.result4);
        }

        [TestMethod()]
        public void ExecuteTest5()
        {
            var ob = GetTestObj();
            Assert.AreEqual(3, ob.result5);
        }

        [TestMethod()]
        public void ExecuteTest7()
        {
            var ob = GetTestObj();
            Assert.AreEqual(4, ob.result7);
        }

        [TestMethod()]
        public void ExecuteTest8()
        {
            var ob = GetTestObj();
            Assert.AreEqual(1024, ob.result8);
        }

        [TestMethod()]
        public void ExecuteTest9()
        {
            var ob = GetTestObj();
            Assert.AreEqual(8.8249778270762871, ob.result9);
        }


        [TestMethod()]
        public void ExecuteTest10()
        {
            var ob = GetTestObj();
            Assert.AreEqual(99, ob.result10);
            Assert.AreEqual(99, ob.expected10);
        }
    }
}