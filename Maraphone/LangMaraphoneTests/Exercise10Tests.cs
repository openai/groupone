﻿using System;
using System.Collections.Generic;
using System.Linq;
using LangMaraphone;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LangMaraphoneTests
{
    [TestClass()]
    public class Exercise10Tests
    {
        public Exercise10 GetTestObj()
        {
            var ret = new Exercise10(false);
            ret.Execute();
            return ret;
        }

        [TestMethod()]
        public void ExecuteTest1()
        {
            var ob = GetTestObj();
            Assert.AreEqual(true, 
                ob.result1.SequenceEqual(
                        Enumerable.Range(0, 101)
                    ));
        }

        [TestMethod()]
        public void ExecuteTest2()
        {
            var ob = GetTestObj();
            Assert.AreEqual(true,
                ob.result2.SequenceEqual(
                    Enumerable.Range(0, 101)
                ));
        }

        [TestMethod()]
        public void ExecuteTest3()
        {
            var ob = GetTestObj();
            Assert.AreEqual(true,
                ob.result3.SequenceEqual(
                    Enumerable.Range(0, 101)
                ));
        }


        [TestMethod()]
        public void ExecuteTest4()
        {
            var ob = GetTestObj();
            Assert.AreEqual(true,
                ob.result4.SequenceEqual(
                    Enumerable.Range(0, 101)
                ));
        }


        [TestMethod()]
        public void ExecuteTest5()
        {
            var ob = GetTestObj();
            Assert.AreEqual(true,
                ob.result5.SequenceEqual(
                    Enumerable.Range(1, 10)
                        .Select(e => new { A = Enumerable.Range(1, 10) })
                        .SelectMany(e => e.A)
                ));
        }

        [TestMethod()]
        public void ExecuteTest6()
        {
            var ob = GetTestObj();
            Assert.AreEqual(true,
                ob.result6.SequenceEqual(
                    Enumerable.Range(1, 100)
                        .Select(e => new { A = Enumerable.Range(1, 3) })
                        .SelectMany(e => e.A)
                        .Take(100)
                ));
        }

        [TestMethod()]
        public void ExecuteTest7()
        {
            var ob = GetTestObj();
            Assert.AreEqual(true,
                ob.result7.SequenceEqual(
                    Enumerable.Range(1, 100)
                        .Select(e => new { A = Enumerable.Range(1, e) })
                        .SelectMany(e => e.A)
                        .Take(100)
                ));
        }

        [TestMethod()]
        public void ExecuteTest8()
        {
            var ob = GetTestObj();
            Assert.AreEqual(true,
                ob.result8.SequenceEqual(
                    Enumerable.Range(1, 100)
                    .Select(e => new { A = Enumerable.Range(1, e) })
                    .SelectMany(e => e.A)
                    .Take(100)
                ));
        }

        [TestMethod()]
        public void ExecuteTest9()
        {
            var seq = 
                new List<int> {2315, 2351, 2517, 2571, 3215,
                    3251, 3417, 3471, 4317, 4371, 5217, 5271 };
            var ob = GetTestObj();
            Assert.AreEqual(true,
                ob.result9.SequenceEqual(
                    seq
                ));
        }

        [TestMethod()]
        public void ExecuteTest10()
        {
            var list = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 9, 9, 9, 9, 9, 9, 8, 7, 6, 5, 4, 3, 2, 2, 2, 2, 2, 2, 2, 3, 4, 5, 6, 7, 8, 8, 8, 8, 8, 8, 7, 6, 5, 4, 3, 3, 3, 3, 3, 4, 5, 6, 7, 7, 7, 7, 6, 5, 4, 4, 4, 5, 6, 6, 5 };
            Assert.AreEqual(true,
                Exercise10.GetSpiral(10).SequenceEqual(
                    list
                ));
        }
    }
}