﻿using LangMaraphone;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LangMaraphoneTests
{
    [TestClass()]
    public class Exercise3Tests
    {
        public Exercise3 GetTestObj()
        {
            var ret = new Exercise3(false);
            ret.Execute();
            return ret;
        }
        
        
        [TestMethod()]
        public void ExecuteTest1()
        {
            var ob = GetTestObj();
            Assert.AreEqual(0.0, ob.expected1);
            Assert.AreEqual(false, ob.match1);
            Assert.AreEqual(1F / 10F * 10F - 1F, ob.expected1);
        }

        [TestMethod()]
        public void ExecuteTest2()
        {
            var ob = GetTestObj();

            Assert.AreEqual(1.0, ob.expected2);
            Assert.AreEqual(false, ob.match2);
            Assert.AreEqual(0.0, ob.expected2_1);
            Assert.IsTrue(1 < ob.result2 && 1.2 > ob.result2);
            Assert.AreEqual(0.0, ob.expected2_2);
            Assert.AreEqual(false, ob.match2_2);
        }

        [TestMethod()]
        public void ExecuteTest3()
        {
            var ob = GetTestObj();
            Assert.AreEqual(0, ob.expected3);
            Assert.AreEqual(0, ob.result3);
            Assert.AreEqual(true, ob.match3);
        }

        [TestMethod()]
        public void ExecuteTest4()
        {
            var ob = GetTestObj();
            Assert.AreEqual(0, ob.result4);
            Assert.AreEqual(1, ob.expected4);
            Assert.AreEqual(true, ob.match4);
        }
    }
}