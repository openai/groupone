﻿using System.Linq;
using LangMaraphone;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LangMaraphoneTests
{
    [TestClass()]
    public class Exercise5Tests
    {
        public Exercise5 GetTestObj()
        {
            var ret = new Exercise5(false);
            ret.Execute();
            return ret;
        }

        [TestMethod()]
        public void ExecuteTest1()
        {
            var ob = GetTestObj();
            Assert.AreEqual(2, ob.result1);
        }

        [TestMethod()]
        public void ExecuteTest2()
        {
            var ob = GetTestObj();
            Assert.AreEqual(2, ob.result1);
        }

        [TestMethod()]
        public void ExecuteTest3()
        {
            Assert.AreEqual(0, Exercise5.Func3(false, false));
            Assert.AreEqual(1, Exercise5.Func3(false, true));
            Assert.AreEqual(2, Exercise5.Func3(true, false));
            Assert.AreEqual(3, Exercise5.Func3(true, true));

        }

        [TestMethod()]
        public void ExecuteTest4()
        {
            Assert.AreEqual(Exercise5.StrangeAnimal.BlueTwoTail, Exercise5.Func4(false, false, false));
            Assert.AreEqual(Exercise5.StrangeAnimal.BlueTwoHorn, Exercise5.Func4(false, false, true));
            Assert.AreEqual(Exercise5.StrangeAnimal.BlueOneTail, Exercise5.Func4(false, true, false));
            Assert.AreEqual(Exercise5.StrangeAnimal.BlueOneHorn, Exercise5.Func4(false, true, true));
            Assert.AreEqual(Exercise5.StrangeAnimal.RedTwoTail, Exercise5.Func4(true, false, false));
            Assert.AreEqual(Exercise5.StrangeAnimal.RedTwoHorn, Exercise5.Func4(true, false, true));
            Assert.AreEqual(Exercise5.StrangeAnimal.RedOneTail, Exercise5.Func4(true, true, false));
            Assert.AreEqual(Exercise5.StrangeAnimal.RedOneHorn, Exercise5.Func4(true, true, true));
        }
    }
}