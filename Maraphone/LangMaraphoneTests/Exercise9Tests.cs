﻿using System;
using System.Linq;
using LangMaraphone;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LangMaraphoneTests
{
    [TestClass()]
    public class Exercise9Tests
    {
        public Exercise9 GetTestObj()
        {
            var ret = new Exercise9(false);
            ret.Execute();
            return ret;
        }

        [TestMethod()]
        public void ExecuteTest1()
        {
            Assert.AreEqual(9, Exercise9.MyMax(1,2,9));
            Assert.AreEqual(2, Exercise9.MyMax(-100, 2, -9));
            Assert.AreEqual(2, Exercise9.MyMax(2, 0, -9));
            Assert.AreEqual(2, Exercise9.MyMax(2, 2, 2));
        }

        [TestMethod()]
        public void ExecuteTest2()
        {
            Assert.AreEqual(Exercise5.StrangeAnimal.BlueTwoTail, Exercise9.Func2(false, false, false));
            Assert.AreEqual(Exercise5.StrangeAnimal.BlueTwoHorn, Exercise9.Func2(false, false, true));
            Assert.AreEqual(Exercise5.StrangeAnimal.BlueOneTail, Exercise9.Func2(false, true, false));
            Assert.AreEqual(Exercise5.StrangeAnimal.BlueOneHorn, Exercise9.Func2(false, true, true));
            Assert.AreEqual(Exercise5.StrangeAnimal.RedTwoTail, Exercise9.Func2(true, false, false));
            Assert.AreEqual(Exercise5.StrangeAnimal.RedTwoHorn, Exercise9.Func2(true, false, true));
            Assert.AreEqual(Exercise5.StrangeAnimal.RedOneTail, Exercise9.Func2(true, true, false));
            Assert.AreEqual(Exercise5.StrangeAnimal.RedOneHorn, Exercise9.Func2(true, true, true));
        }

        [TestMethod()]
        public void ExecuteTest3()
        {
            Assert.AreEqual(Exercise5.StrangeAnimal.BlueTwoTail, Exercise9.Func3(false, false, false));
            Assert.AreEqual(Exercise5.StrangeAnimal.BlueTwoHorn, Exercise9.Func3(false, false, true));
            Assert.AreEqual(Exercise5.StrangeAnimal.BlueOneTail, Exercise9.Func3(false, true, false));
            Assert.AreEqual(Exercise5.StrangeAnimal.BlueOneHorn, Exercise9.Func3(false, true, true));
            Assert.AreEqual(Exercise5.StrangeAnimal.RedTwoTail, Exercise9.Func3(true, false, false));
            Assert.AreEqual(Exercise5.StrangeAnimal.RedTwoHorn, Exercise9.Func3(true, false, true));
            Assert.AreEqual(Exercise5.StrangeAnimal.RedOneTail, Exercise9.Func3(true, true, false));
            Assert.AreEqual(Exercise5.StrangeAnimal.RedOneHorn, Exercise9.Func3(true, true, true));
        }


        [TestMethod()]
        public void ExecuteTest4()
        {
            CheckAnimal(Exercise5.StrangeAnimal.BlueTwoTail);
            CheckAnimal(Exercise5.StrangeAnimal.BlueTwoHorn);
            CheckAnimal(Exercise5.StrangeAnimal.BlueOneTail);
            CheckAnimal(Exercise5.StrangeAnimal.BlueOneHorn);

            CheckAnimal(Exercise5.StrangeAnimal.RedTwoTail);
            CheckAnimal(Exercise5.StrangeAnimal.RedTwoHorn);
            CheckAnimal(Exercise5.StrangeAnimal.RedOneTail);
            CheckAnimal(Exercise5.StrangeAnimal.RedOneHorn);
        }

        private static void CheckAnimal(Exercise5.StrangeAnimal an)
        {
            bool isRed, isOne, isHorn;
            Exercise9.Func4(an, out isRed, out isOne, out isHorn);
            Assert.AreEqual(an, Exercise9.Func3(isRed, isOne, isHorn));
        }
    }
}