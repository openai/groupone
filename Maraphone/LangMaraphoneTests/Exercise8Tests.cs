﻿using System;
using System.Linq;
using LangMaraphone;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LangMaraphoneTests
{
    [TestClass()]
    public class Exercise8Tests
    {
        public Exercise8 GetTestObj()
        {
            var ret = new Exercise8(false);
            ret.Execute();
            return ret;
        }

        [TestMethod()]
        public void ExecuteTest1()
        {
            var ob = GetTestObj();
            Assert.AreEqual("10", ob.MyIntToString(10));
            Assert.AreEqual("9999", ob.MyIntToString(9999));
        }

        [TestMethod()]
        public void ExecuteTest2()
        {
            var ob = GetTestObj();
            Assert.AreEqual("10.000", ob.MyDoubleToString(10.0));
            Assert.AreEqual("12.321", ob.MyDoubleToString(12.321321));
            Assert.AreEqual("0.000", ob.MyDoubleToString(0.0001));
        }

        [TestMethod()]
        public void ExecuteTest3()
        {
            var ob = GetTestObj();
            Assert.AreEqual("10.00 %", ob.MyDoubleToPercentString(0.1));
            Assert.AreEqual("210.00 %", ob.MyDoubleToPercentString(2.1));
            Assert.AreEqual("0.10 %", ob.MyDoubleToPercentString(0.001));
            Assert.AreEqual("0.00 %", ob.MyDoubleToPercentString(0.00001));
        }

        [TestMethod()]
        public void ExecuteTest4()
        {
            var ob = GetTestObj();
            Assert.AreEqual("0000010", ob.MyIntToPreString(10));
            Assert.AreEqual("0009999", ob.MyIntToPreString(9999));
            Assert.AreEqual("32119999", ob.MyIntToPreString(32119999));
        }

        [TestMethod()]
        public void ExecuteTest5()
        {
            var ob = GetTestObj();
            Assert.AreEqual(10, ob.MyStringToInt("10"));
            Assert.AreEqual(9999, ob.MyStringToInt("9999"));
        }

        [ExpectedException(typeof(FormatException))]
        [TestMethod()]
        public void ExecuteTest51()
        {
            var ob = GetTestObj();
            ob.MyStringToInt("a");
        }

        [TestMethod()]
        public void ExecuteTest6()
        {
            var ob = GetTestObj();
            bool scss1;
            bool scss2;
            Assert.AreEqual(10, ob.MyStringToInt("10", out scss1));
            Assert.AreEqual(true, scss1);
            ob.MyStringToInt("a", out scss2);
            Assert.AreEqual(false, scss2);
        }
    }
}